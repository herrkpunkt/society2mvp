import { terser } from "rollup-plugin-terser";
import copy from 'rollup-plugin-copy';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';

const variants = ["wasm", "js"];
const formats = ["umd", "es"];

const outputs = variants.reduce(
  (acc, variant) => [
    ...acc,
    {
	input: `society2/javascript/src/index_${variant}.js`,
	plugins: [
	    commonjs({
              include: 'node_modules/**'
            }
	    ),
	    terser(),
	    resolve({
		browser: true,

            }),
	output: formats.reduce(
            (acc, format) => [
		...acc,
		{
		    file: `society2/javascript/dist/${variant}/${format}/index.js`,
		    sourcemap: true,
		    format,
		    name: "Society2",
		    plugins: [terser(), 
		 ],

          },
        ],
        []
      ),
    },
  ],
  []
);

export default outputs;
