export const createLoader = async (bin) => {
  const library = await bin();
  return {
    library,
  };
};
