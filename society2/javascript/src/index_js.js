import society2JsLibrary from "../bin/society2-js";

import { createLoader } from "./loader";

import { MAMImpl } from "./implementation/mam";

export default async () => {
  const { library } = await createLoader(society2JsLibrary);

  return {
    MAM: MAMImpl(library),
  };
};
