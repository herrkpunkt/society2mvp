import society2WasmLibrary from "../bin/society2-wasm";

import { createLoader } from "./loader";

import { MAMImpl } from "./implementation/mam";

export default async () => {
  const { library } = await createLoader(society2WasmLibrary);
  return {
      MAM: MAMImpl(library),
  };
};
