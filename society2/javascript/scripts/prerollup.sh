#This file is part of MAM-Processor.
#
#    MAM-Processor is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    MAM-Processor is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with MAM-Processor. If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env bash
set -euo pipefail
rm -rf psi_cardinality/javascript/bin/*
rm -rf psi_cardinality/javascript/dist/*
cp -rf bazel-out/wasm-opt/bin/psi_cardinality/javascript/psi_cardinality_client_js.js psi_cardinality/javascript/bin/
cp -rf bazel-out/wasm-opt/bin/psi_cardinality/javascript/psi_cardinality_client_wasm.js psi_cardinality/javascript/bin/
cp -rf bazel-out/wasm-opt/bin/psi_cardinality/javascript/psi_cardinality_server_js.js psi_cardinality/javascript/bin/
cp -rf bazel-out/wasm-opt/bin/psi_cardinality/javascript/psi_cardinality_server_wasm.js psi_cardinality/javascript/bin/
