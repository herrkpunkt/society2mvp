//This file is part of MAM-Processor.
//
//    MAM-Processor is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    MAM-Processor is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with MAM-Processor. If not, see <http://www.gnu.org/licenses/>.

#include <emscripten/bind.h>
#include "society2/cpp/mam.h"

using namespace emscripten;
using namespace Society2;

EMSCRIPTEN_BINDINGS(Society2) {
    emscripten::class_<MAM>("MAM")
        .constructor<>()
        .class_function("encrypt", &MAM::encrypt)
        .class_function("decrypt", &MAM::decrypt)
        .class_function("getNTRUPubKey", &MAM::getNTRUPubKey)
        .class_function("generateReaderAPI", &MAM::generateReaderAPI)
        .class_function("generatePublisherAPI", &MAM::generatePublisherAPI)
        .class_function("addTrustedNTRUPubKey", &MAM::addTrustedNTRUPubKey)
        .class_function("addTrustedChannel", &MAM::addTrustedChannel)
        .class_function("getTrustedChannels", &MAM::getTrustedChannels)
        .class_function("addTrustedEndpoint", &MAM::addTrustedEndpoint)
        .class_function("getTrustedEndpoints", &MAM::getTrustedEndpoints)
        .class_function("getWorkingChannelConfig", &MAM::getWorkingChannelConfig)
        .class_function("removeTrustedNTRUPubKey", &MAM::removeTrustedNTRUPubKey)
        .class_function("getTrustedNTRUPubKeys", &MAM:: getTrustedNTRUPubKeys)
        .class_function("getChannels", &MAM:: getChannels)
        .class_function("getVector", &MAM::getVector);
    register_vector<std::string>("vector<std::string>");
    register_vector<std::vector<std::string>>("vector<std::vector<std::string>>");
}
