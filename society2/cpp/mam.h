//This file is part of MAM-Processor.
//
//    MAM-Processor is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    MAM-Processor is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with MAM-Processor. If not, see <http://www.gnu.org/licenses/>.

#ifndef MAM_H
#define MAM_H

#include <string>
#include <random>
#include <vector>
#include <stdio.h>
#include <algorithm>
#include <chrono>

#include "common/trinary/trit_tryte.h"
#include "common/trinary/tryte.h"
#include "common/trinary/tryte_ascii.h"
#include "common/crypto/kerl/kerl.h"
#include "mam/api/api.h"
#include "mam/mam/mam_channel_t_set.h"
#include "mam/ntru/ntru.h"

#include "gzip.h"

#include "base64.h"

namespace Society2{
  class MAM{
    public:
      //Get active channel and endpoint of the given API object (api), do channel maintenance and encrypt the given message (msg). Return updated API object (0,0) and all bundles (1-n, 0-m) that accrued during maintenance and encryption process in the correct order.
    static std::vector<std::vector<std::string>> encrypt(const std::string &msg, const std::string &api, const bool &pub, const std::string &key, const std::string &psk);
      //Decrypt given bundle (msg) in the context of given API object (api). Return updated API object (0) and the decrypted message (1).
      static std::vector<std::string> decrypt(const std::vector<std::string> &msg, const std::string &api);
      //Get the NTRU public key in the context of the given API object (api). Return the NTRU public key.
      static std::string getNTRUPubKey(const std::string &api);
      //Generate a new Reader API from the given publisher API object (publisher_api). NTRU keypair is copied from the publisher API object, given channel public key (channel) is added to the list of trusted channels. Given psk is added. Return the newly created API object.
      static std::string generateReaderAPI(const std::string &publisher_api, const std::string &channel, const std::string &psk);
      //Generate a new publisher API object (api).A  seed and a NTRU keypair are randomly generated and a channel and an endpoint are generated from this seed. A PSK is added for the initial endpoint announcement bundle to be secure. Return the newly created API object (0,0) and the bundle containing the endpoint announcement (1,0-2).
      static std::vector<std::vector<std::string>> generatePublisherAPI(const std::string &entropy, const std::string &psk);
      //Get trusted NTRU public keys used for encryption in the context of given API object (api). Return list of keys
      static std::vector<std::string> getTrustedNTRUPubKeys(const std::string &api);
      //Add a trusted NTRU public key (ntru) to the context of the given API object (api). Return the updated API object.
      static std::string addTrustedNTRUPubKey(const std::string &api, const std::string &ntru);
      //Remove a trusted NTRU public key (ntru) from the context of the given API object (api). Return the updated API object.
      static std::string removeTrustedNTRUPubKey(const std::string &api, const std::string &ntru);
      //Add a trusted channel public key (channel) to the context of the given API object (api). Return the updated API object.
      static std::string addTrustedChannel(const std::string &api, const std::string &channel);
      //Add a trusted endpoint public key (endpoint) to the context of the given API object (api). Return the updated API object.
      static std::string addTrustedEndpoint(const std::string &api, const std::string &endpoint);
      //Get list of trusted channels in the context of the given API object (api). Return list of trusted channels.
      static std::vector<std::string> getTrustedChannels(const std::string &api);
      //Get list of trusted endpoints for a given channel in the context of the given API object (api). Return list of trusted endpoints.
      static std::vector<std::string> getTrustedEndpoints(const std::string &api);
      //Get active channel public key in the context of the given API object (api). Return the channel public key.
      static std::vector<std::vector<std::string>> getWorkingChannelConfig(const std::string &api);
      //Get all channel public keys in the context of the given API object (api). Return the channels.
      static std::vector<std::string> getChannels(const std::string &api);
      //Get empty vector
      static std::vector<std::string> getVector();
  };
} // namespace Society2
#endif // MAM_H
