//This file is part of MAM-Processor.
//
//    MAM-Processor is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    MAM-Processor is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with MAM-Processor. If not, see <http://www.gnu.org/licenses/>.

#define IOTA_TRYTE_ALPHABET "ABCDEFGHIJKLMNOPQRSTUVWXYZ9"
#define API_PATH "api.dat"
#define EMPTY_SEED "999999999999999999999999999999999999999999999999999999999999999999999999999999999"
#define TRYTES_PER_CHAR 2
#define TRITS_PER_TRYTE 3
#define MSS_DEPTH_CHANNEL 3
#define MSS_DEPTH_ENDPOINT 3

#include "mam.h"

const std::string generateTrytes(unsigned int _size, const std::string &entropy) {
  const unsigned int seed_time = std::chrono::system_clock::now().time_since_epoch().count();
  std::string seedstring = std::to_string(seed_time);
  seedstring = seedstring.append(entropy);
  std::seed_seq seed (seedstring.begin(),seedstring.end());
  std::mt19937 generator(seed);
  std::string result = std::string();
  int random = 0;
  for (unsigned int i = 0; i < _size; i++) {
    random = generator() % 27;
    result += IOTA_TRYTE_ALPHABET[random];
  }
  return result;
}

std::vector<std::string> bundleToVec(bundle_transactions_t * _bundle){
  retcode_t ret = RC_ERROR;
  std::vector<std::string> result;
  size_t bundle_size = bundle_transactions_size(_bundle);
  for(unsigned int i = 0; i < bundle_size; i++){
    iota_transaction_t * temp_tx = bundle_at(_bundle, i);
    flex_trit_t * tx_trits = transaction_serialize(temp_tx);
    tryte_t * tx_trytes = (tryte_t *)malloc(NUM_TRYTES_SERIALIZED_TRANSACTION);
    flex_trits_to_trytes(tx_trytes, NUM_TRYTES_SERIALIZED_TRANSACTION, tx_trits, NUM_TRITS_SERIALIZED_TRANSACTION, NUM_TRITS_SERIALIZED_TRANSACTION);
    std::string tx_tryte_string = "";
      for (unsigned int j = 0; j < NUM_TRYTES_SERIALIZED_TRANSACTION; j++) {
        tx_tryte_string += (char)tx_trytes[j];
      }
      result.push_back(tx_tryte_string);
  }
  bundle_transactions_free(&_bundle);
  return result;
}

bundle_transactions_t * vecToBundle(std::vector<std::string> _vec){
  bundle_transactions_t *bundle = NULL;
  bundle_transactions_new(&bundle);
  for(unsigned int i = 0; i < _vec.size(); i++){
    tryte_t * tx_trytes = (tryte_t *)malloc(NUM_TRYTES_SERIALIZED_TRANSACTION);
    for(unsigned int j = 0; j < NUM_TRYTES_SERIALIZED_TRANSACTION; j++){
      tx_trytes[j] = (tryte_t)_vec.at(i).at(j);
    }

    size_t flex_trits_size = NUM_FLEX_TRITS_FOR_TRITS(NUM_TRITS_SERIALIZED_TRANSACTION);
    flex_trit_t * tx_trits = (flex_trit_t *)malloc(flex_trits_size);
    flex_trits_from_trytes(tx_trits, NUM_TRITS_SERIALIZED_TRANSACTION, tx_trytes, NUM_TRITS_SERIALIZED_TRANSACTION, NUM_TRYTES_SERIALIZED_TRANSACTION);
    free(tx_trytes);
    iota_transaction_t * temp_tx = transaction_deserialize(tx_trits, true);
    free(tx_trits);
    bundle_transactions_add(bundle, temp_tx);
  }
  retcode_t bundlevalidityret = RC_ERROR;
  bundle_status_t bundlestate = BUNDLE_NOT_INITIALIZED;
  bundlevalidityret = bundle_validate(bundle, &bundlestate);
  return bundle;
}

void finalizeBundle(bundle_transactions_t * _bundle){
  Kerl kerl;
  kerl_init(&kerl);
  bundle_finalize(_bundle, &kerl);
  return;
}

void saveAPIToFile(const std::string &api){
  std::string base64decoded = base64_decode(api);
  std::string decompressed = Gzip::decompress(base64decoded);
  FILE *file = NULL;
  std::string filename = API_PATH;
  file = fopen(filename.c_str(), "w+");
  fwrite(decompressed.c_str(), sizeof(char), decompressed.length(), file);
  fclose(file);
  return;
}

std::string extractAPIFromFile(){
  FILE *file = NULL;
  file = fopen(API_PATH, "r");
  fseek(file, 0, SEEK_END);
  size_t bytes_size = 0;
  bytes_size = ftell(file);
  fseek(file, 0, SEEK_SET);
  char *bytes = NULL;
  bytes = (char *)malloc(bytes_size);
  fread(bytes, sizeof(char), bytes_size, file);
  fclose(file);
  std::string compressed = Gzip::compress(std::string(bytes, bytes_size));
  free(bytes);
  std::string encoded = base64_encode(reinterpret_cast<const unsigned char *>(compressed.c_str()), compressed.length());
  return encoded;
}

std::vector<std::tuple<std::string, int, std::vector<std::tuple<std::string, int>>>> getChannelConfig(){
  std::vector<std::tuple<std::string, int, std::vector<std::tuple<std::string, int>>>>result;
  result.clear();
  mam_api_t _api;
  mam_api_load(API_PATH, &_api, NULL, 0);
  mam_channel_t_set_entry_t *entry = NULL;
  mam_channel_t_set_entry_t *tmp = NULL;
  SET_ITER(_api.channels, entry, tmp) {
    tryte_t channel_temp[MAM_CHANNEL_ID_TRYTE_SIZE];
    mam_endpoint_t_set_entry_t *epentry = NULL;
    mam_endpoint_t_set_entry_t *eptmp = NULL;
    trits_to_trytes(trits_begin(mam_channel_id(&entry->value)), channel_temp, MAM_CHANNEL_ID_TRIT_SIZE);
    int remskschannel = mam_api_channel_remaining_sks(&_api, channel_temp);
    std::string channel_s = "";
    for (unsigned int i = 0; i < MAM_CHANNEL_ID_TRYTE_SIZE; i++) {
      channel_s += (char)channel_temp[i];
    }
    std::vector<std::tuple<std::string, int>> endpoints_v;
    endpoints_v.clear();
    SET_ITER(entry->value.endpoints, epentry, eptmp) {
      tryte_t ep_temp[MAM_ENDPOINT_ID_TRYTE_SIZE];
      trits_to_trytes(trits_begin(mam_endpoint_id(&epentry->value)), ep_temp, MAM_ENDPOINT_ID_TRIT_SIZE);
      int remsksendpoint = mam_api_endpoint_remaining_sks(&_api, channel_temp, ep_temp);
      std::string ep_s = "";
      for (unsigned int i = 0; i < MAM_ENDPOINT_ID_TRYTE_SIZE; i++) {
        ep_s += (char)ep_temp[i];
      }
      endpoints_v.push_back(std::make_tuple(ep_s, remsksendpoint));
    }
    result.push_back(std::make_tuple(channel_s, remskschannel, endpoints_v));
  }
  mam_api_destroy(&_api);
  return result;
}

namespace Society2{

  std::vector<std::vector<std::string>> MAM::getWorkingChannelConfig(const std::string &api) {
    saveAPIToFile(api);
    std::vector<std::vector<std::string>> result;
    result.clear();
    std::vector<std::vector<std::string>> bundles;
    bundles.clear();
    std::vector<std::tuple<std::string, int, std::vector<std::tuple<std::string, int>>>> channelConfig = getChannelConfig();
    std::tuple<std::string, int, std::vector<std::tuple<std::string, int>>> latestChannel = channelConfig.at(channelConfig.size() - 1);
    std::tuple<std::string, int> latestEndpoint = std::make_tuple("INVALID", 0);
    if (std::get<2>(latestChannel).size() > 0) latestEndpoint = std::get<2>(latestChannel).at(std::get<2>(latestChannel).size() - 1);
    if (std::get<1>(latestEndpoint) < 2) {
      mam_api_t _api;
      mam_api_load(API_PATH, &_api, NULL, 0);
      tryte_t channel_id[81];
      tryte_t endpoint_id[81];
      tryte_t new_channel_id[81];
      bool channelGenerated = false;
      for (unsigned int i = 0; i < 81; i++) {
        channel_id[i] = (tryte_t)std::get<0>(latestChannel)[i];
      }
      if (std::get<1>(latestChannel) < 3) {
        mam_api_channel_create(&_api, MSS_DEPTH_CHANNEL, new_channel_id);
        trit_t channel_message_id[MAM_MSG_ID_SIZE];
        bundle_transactions_t *channelbundle = NULL;
        bundle_transactions_new(&channelbundle);
        mam_api_bundle_announce_channel(&_api, channel_id, new_channel_id, _api.psks, NULL, channelbundle, channel_message_id);
        channelGenerated = true;
	finalizeBundle(channelbundle);
        bundles.push_back(bundleToVec(channelbundle));
      }
      if (channelGenerated) mam_api_endpoint_create(&_api, MSS_DEPTH_ENDPOINT, new_channel_id, endpoint_id);
      else mam_api_endpoint_create(&_api, MSS_DEPTH_ENDPOINT, channel_id, endpoint_id);
      trit_t endpoint_message_id[MAM_MSG_ID_SIZE];
      bundle_transactions_t *endpointbundle = NULL;
      bundle_transactions_new(&endpointbundle);
      if(channelGenerated) mam_api_bundle_announce_endpoint(&_api, new_channel_id, endpoint_id, _api.psks, NULL, endpointbundle, endpoint_message_id);
      else mam_api_bundle_announce_endpoint(&_api, channel_id, endpoint_id, _api.psks, NULL, endpointbundle, endpoint_message_id);
      finalizeBundle(endpointbundle);
      bundles.push_back(bundleToVec(endpointbundle));
      mam_api_save(&_api, API_PATH, NULL, 0);
      mam_api_destroy(&_api);
    }
    std::vector<std::string> theAPI;
    theAPI.push_back(extractAPIFromFile());
    result.push_back(theAPI);
    channelConfig = getChannelConfig();
    mam_api_t _api;
    mam_api_load(API_PATH, &_api, NULL, 0);
    tryte_t channel_id[81];
    tryte_t endpoint_id[81];
    for (unsigned int i = 0; i < 81; i++) {
      channel_id[i] = (tryte_t)std::get<0>(channelConfig.at(channelConfig.size() - 1))[i];
      endpoint_id[i] = (tryte_t)std::get<0>(
      std::get<2>(channelConfig.at(channelConfig.size() - 1)).at(std::get<2>(channelConfig.at(channelConfig.size() - 1)).size() - 1))[i];
    }
    std::string chan;
    std::string ep;
    for (unsigned int i = 0; i < MAM_CHANNEL_ID_TRYTE_SIZE; i++) {
      chan += (char)channel_id[i];
      ep += (char)endpoint_id[i];
    }
    std::vector<std::string> config;
    config.clear();
    config.push_back(chan);
    config.push_back(ep);
    result.push_back(config);
    for(unsigned int i = 0; i < bundles.size(); i++){
      result.push_back(bundles.at(i));
    }
    mam_api_destroy(&_api);
    return result;
  }

  std::vector<std::string> MAM::getChannels(const std::string &api){
    std::vector<std::string> result;
    saveAPIToFile(api);
    mam_api_t _api;
    mam_api_load(API_PATH, &_api, NULL, 0);
    std::vector<std::string> own_channels;
    mam_channel_t_set_entry_t *own_entry = NULL;
    mam_channel_t_set_entry_t *own_tmp = NULL;
    SET_ITER(_api.channels, own_entry, own_tmp) {
      tryte_t own_channel_temp[MAM_CHANNEL_ID_TRYTE_SIZE];
      trits_to_trytes(trits_begin(mam_channel_id(&own_entry->value)), own_channel_temp, MAM_CHANNEL_ID_TRIT_SIZE);
      std::string own_channel_s = "";
      for (unsigned int i = 0; i < MAM_CHANNEL_ID_TRYTE_SIZE; i++) {
        own_channel_s += (char)own_channel_temp[i];
      }
      result.push_back(own_channel_s);
    }
    mam_api_destroy(&_api);
    return result;
  }
  
  std::vector<std::vector<std::string>> MAM::encrypt(const std::string &msg, const std::string &api, const bool &pub, const std::string &key, const std::string &psk){
    saveAPIToFile(api);
    std::vector<std::vector<std::string>> res;
    res.clear();
    std::string compressed_msg = Gzip::compress(msg);
    std::string compressed_encoded_msg = base64_encode(reinterpret_cast<const unsigned char *>(compressed_msg.c_str()), compressed_msg.length());
    size_t size = strlen(compressed_encoded_msg.c_str()) * TRYTES_PER_CHAR;
    tryte_t buffer[size];
    ascii_to_trytes(compressed_encoded_msg.c_str(), buffer);
    bundle_transactions_t *bundle = NULL;
    bundle_transactions_new(&bundle);
    trit_t message_id[MAM_MSG_ID_SIZE];
    std::vector<std::vector<std::string>> cfgnow = getWorkingChannelConfig(api);
    mam_api_t _api;
    mam_api_load(API_PATH, &_api, NULL, 0);
    if(!pub){
      if(key == ""){
	printf("encrypting for all keys\n");
        mam_api_bundle_write_header_on_endpoint(&_api, (tryte_t *)cfgnow.at(1).at(0).c_str(), (tryte_t *)cfgnow.at(1).at(1).c_str(), NULL, _api.ntru_pks, bundle, message_id);
      }
      else{
	printf("encrypting for one key\n");
        mam_api_t temp_api;
        mam_api_init(&temp_api, (tryte_t *)EMPTY_SEED);
        mam_ntru_pk_t new_pk;
        trit_t key_trit_t[MAM_NTRU_PK_SIZE];
        trytes_to_trits((tryte_t *)key.c_str(), key_trit_t,key.length());
        for (unsigned int i = 0; i < MAM_NTRU_PK_SIZE; i++) {
          new_pk.key[i] = key_trit_t[i];
        }
        mam_api_add_ntru_pk(&temp_api, &new_pk);
        mam_api_bundle_write_header_on_endpoint(&_api, (tryte_t *)cfgnow.at(1).at(0).c_str(), (tryte_t *)cfgnow.at(1).at(1).c_str(), NULL, temp_api.ntru_pks, bundle, message_id);
        mam_api_destroy(&temp_api);
      }
    }
    else{
      printf("encrypting for zero keys\n");
      mam_api_bundle_write_header_on_endpoint(&_api, (tryte_t *)cfgnow.at(1).at(0).c_str(), (tryte_t *)cfgnow.at(1).at(1).c_str(), NULL, NULL, bundle, message_id);
    }
    mam_api_bundle_write_packet(&_api, message_id, buffer, size, MAM_MSG_CHECKSUM_SIG, true, bundle);
    mam_api_save(&_api, API_PATH, NULL, 0);
    mam_api_destroy(&_api);
    std::vector<std::string> theAPI;
    theAPI.push_back(extractAPIFromFile());
    res.push_back(theAPI);
    for(unsigned int i = 2; i < cfgnow.size(); i++){
      res.push_back(cfgnow.at(i));
    }
    finalizeBundle(bundle);
    res.push_back(bundleToVec(bundle));
    return res;
  }
  
  std::vector<std::string> MAM::decrypt(const std::vector<std::string> &msg, const std::string &api){
    std::vector<std::string> res;
    retcode_t ret;
    saveAPIToFile(api);
    mam_api_t _api;
    ret = mam_api_load(API_PATH, &_api, NULL, 0);
    tryte_t *payload = NULL;
    size_t payload_size = 0;
    bool is_last_packet;
    std::string final_payload = "";
    bundle_transactions_t *bundle = vecToBundle(msg);
    ret = mam_api_bundle_read(&_api, bundle, &payload, &payload_size, &is_last_packet);
    if (payload_size != 0) {
      size_t size = payload_size / 2;
      char buffer[size];
      trytes_to_ascii(payload, payload_size, buffer);
      std::string payload_s(buffer, size);
      std::string b64decoded_payload_s = base64_decode(payload_s);
      std::string uncompressed_payload = Gzip::decompress(b64decoded_payload_s);
      final_payload = uncompressed_payload;
    }
    else{
    }
    bundle_transactions_free(&bundle);
    free(payload);
    mam_api_save(&_api, API_PATH, NULL, 0);
    mam_api_destroy(&_api);
    res.push_back(extractAPIFromFile());
    res.push_back(final_payload);
    return res;
  }
  
  std::string MAM::getNTRUPubKey(const std::string &api){
    saveAPIToFile(api);
    mam_api_t _api;
    mam_api_load(API_PATH, &_api, NULL, 0);
    mam_ntru_sk_t_set_entry_t *entry;
    mam_ntru_sk_t_set_entry_t *tmp;
    std::vector<std::string> result;
    SET_ITER(_api.ntru_sks, entry, tmp) {
      tryte_t pk_temp[MAM_NTRU_PK_SIZE/3];
      trits_to_trytes(trits_begin(ntru_sk_pk_key(&entry->value)), pk_temp, MAM_NTRU_PK_SIZE);
      std::string pkres = "";
      for (unsigned int i = 0; i < MAM_NTRU_PK_SIZE / 3; i++) {
        pkres += (char)pk_temp[i];
      }
      result.push_back(pkres);
    }
    mam_api_destroy(&_api);
    return result.at(0);
  }
  
  std::vector<std::string> MAM::getTrustedChannels(const std::string &api){
    saveAPIToFile(api);
    mam_api_t _api;
    mam_api_load(API_PATH, &_api, NULL, 0);
    std::vector<std::string> own_channels;
    mam_channel_t_set_entry_t *own_entry = NULL;
    mam_channel_t_set_entry_t *own_tmp = NULL;
    SET_ITER(_api.channels, own_entry, own_tmp) {
      tryte_t own_channel_temp[MAM_CHANNEL_ID_TRYTE_SIZE];
      trits_to_trytes(trits_begin(mam_channel_id(&own_entry->value)), own_channel_temp, MAM_CHANNEL_ID_TRIT_SIZE);
      std::string own_channel_s = "";
      for (unsigned int i = 0; i < MAM_CHANNEL_ID_TRYTE_SIZE; i++) {
        own_channel_s += (char)own_channel_temp[i];
      }
      own_channels.push_back(own_channel_s);
    }
    std::vector<std::string> result;
    mam_pk_t_set_entry_t *entry;
    mam_pk_t_set_entry_t *tmp;
    SET_ITER(_api.trusted_channel_pks, entry, tmp) {
      std::string pkres = "";
      char arr[MAM_CHANNEL_ID_TRIT_SIZE / 3];
      trits_to_str(trits_from_rep(MAM_CHANNEL_ID_TRIT_SIZE, entry->value.key), arr);
      for (unsigned int i = 0; i < MAM_CHANNEL_ID_TRIT_SIZE / 3; i++) {
        pkres += arr[i];
      }
      if (std::find(own_channels.begin(), own_channels.end(), pkres) == own_channels.end()) {
        result.push_back(pkres);
      }
    }
    mam_api_destroy(&_api);
    return result;
  }
  
  std::vector<std::string> MAM::getTrustedEndpoints(const std::string &api){
    std::vector<std::string> result;
    result.clear();
    saveAPIToFile(api);
    mam_api_t _api;
    mam_api_load(API_PATH, &_api, NULL, 0);
    mam_pk_t_set_entry_t *entry;
    mam_pk_t_set_entry_t *tmp;
    SET_ITER(_api.trusted_endpoint_pks, entry, tmp){
      std::string pkres = "";
      char arr[MAM_CHANNEL_ID_TRIT_SIZE / 3];
      trits_to_str(trits_from_rep(MAM_CHANNEL_ID_TRIT_SIZE, entry->value.key), arr);
      for (unsigned int i = 0; i < MAM_CHANNEL_ID_TRIT_SIZE / 3; i++) {
        pkres += arr[i];
      }
      result.push_back(pkres);
    }
    mam_api_destroy(&_api);
    return result;
  }
  
  std::string MAM::generateReaderAPI(const std::string &publisher_api, const std::string &channel, const std::string &psk){
    saveAPIToFile(publisher_api);
    mam_api_t base_api;
    mam_api_load(API_PATH, &base_api, NULL, 0);
    mam_api_t _api;
    mam_api_init(&_api, (tryte_t *)EMPTY_SEED);
    mam_ntru_sk_t_set_entry_t *entry = NULL;
    mam_ntru_sk_t_set_entry_t *tmp = NULL;
    SET_ITER(base_api.ntru_sks, entry, tmp) {
      mam_api_add_ntru_sk(&_api, &entry->value);
    }
    mam_api_destroy(&base_api);
    mam_api_add_trusted_channel_pk(&_api, (tryte_t *)channel.c_str());
    mam_psk_t _psk;
    trit_t psk_trits[MAM_PSK_KEY_SIZE];
    trytes_to_trits((tryte_t *)psk.c_str(), psk_trits, psk.length());
    for (unsigned int i = 0; i < MAM_PSK_KEY_SIZE; i++) {
      _psk.key[i] = psk_trits[i];
    }
    mam_api_add_psk(&_api, &_psk);
    mam_api_save(&_api, API_PATH, NULL, 0);
    mam_api_destroy(&_api);
    std::string result = extractAPIFromFile();
    return result;
  }
  
  std::vector<std::vector<std::string>> MAM::generatePublisherAPI(const std::string &entropy, const std::string &psk){
    std::vector<std::vector<std::string>> result;
    result.clear();
    mam_api_t _api;
    std::string _seed = generateTrytes(81, entropy);
    std::string _nonce = generateTrytes(9, entropy);
    mam_api_init(&_api, (tryte_t *)_seed.c_str());
    mam_ntru_sk_t ntru;
    MAM_TRITS_DEF(nonce, 3 * 9);
    nonce = MAM_TRITS_INIT(nonce, 3 * 9);
    trits_from_str(nonce, _nonce.c_str());
    ntru_sk_reset(&ntru);
    ntru_sk_gen(&ntru, &_api.prng, nonce);
    mam_api_add_ntru_sk(&_api, &ntru);
    mam_psk_t _psk;
    trit_t psk_trits[MAM_PSK_KEY_SIZE];
    trytes_to_trits((tryte_t *)psk.c_str(), psk_trits, psk.length());
    for (unsigned int i = 0; i < MAM_PSK_KEY_SIZE; i++) {
      _psk.key[i] = psk_trits[i];
    }
    mam_api_add_psk(&_api, &_psk);
    tryte_t channel_id[MAM_CHANNEL_ID_TRYTE_SIZE];
    mam_api_channel_create(&_api, MSS_DEPTH_CHANNEL, channel_id);
    std::string channel_s = "";
    for (unsigned int i = 0; i < MAM_CHANNEL_ID_TRYTE_SIZE; i++) {
      channel_s += (char)channel_id[i];
    }
    tryte_t endpoint_id[MAM_ENDPOINT_ID_TRYTE_SIZE];
    mam_api_endpoint_create(&_api, MSS_DEPTH_ENDPOINT, channel_id, endpoint_id);
    trit_t message_id[MAM_MSG_ID_SIZE];
    bundle_transactions_t *bundle = NULL;
    bundle_transactions_new(&bundle);
    mam_api_bundle_announce_endpoint(&_api, channel_id, endpoint_id, _api.psks, NULL, bundle, message_id);
    mam_api_save(&_api, API_PATH, NULL, 0);
    mam_api_destroy(&_api);
    finalizeBundle(bundle);
    bundle_status_t bundlestate = BUNDLE_NOT_INITIALIZED;
    retcode_t bundlevalidityret = RC_ERROR;
    bundlevalidityret = bundle_validate(bundle, &bundlestate);
    std::vector<std::string> the_endpoint_announcement = bundleToVec(bundle);
    std::vector<std::string> the_api;
    the_api.clear();
    the_api.push_back(extractAPIFromFile());
    result.push_back(the_api);
    result.push_back(the_endpoint_announcement);
    return result;
  }

  std::vector<std::string> MAM::getTrustedNTRUPubKeys(const std::string &api){
    std::vector<std::string> result;
    saveAPIToFile(api);
    mam_api_t _api;
    mam_api_load(API_PATH, &_api, NULL, 0);
    mam_ntru_pk_t_set_entry_t *entry;
    mam_ntru_pk_t_set_entry_t *tmp;
    SET_ITER(_api.ntru_pks, entry, tmp) {
      tryte_t pk_temp[MAM_NTRU_PK_SIZE];
      trits_to_trytes(trits_begin(mam_ntru_pk_key(&entry->value)), pk_temp, MAM_NTRU_PK_SIZE);
      std::string pkres = "";
      for (unsigned int i = 0; i < MAM_NTRU_PK_SIZE / 3; i++) {
        pkres += (char)pk_temp[i];
      }
      result.push_back(pkres);
    }
    mam_api_destroy(&_api);
    return result;
  }
  
  std::string MAM::addTrustedNTRUPubKey(const std::string &api, const std::string &ntru){
    retcode_t ret;
    saveAPIToFile(api);
    mam_api_t _api;
    ret = mam_api_load(API_PATH, &_api, NULL, 0);
    if(ret != RC_OK){
      printf("Error loading API object\n");
    }
    mam_ntru_pk_t new_pk;
    trit_t key_trits[MAM_NTRU_PK_SIZE];
    trytes_to_trits((tryte_t *)ntru.c_str(), key_trits, ntru.length());
    for (unsigned int i = 0; i < MAM_NTRU_PK_SIZE; i++) {
      new_pk.key[i] = key_trits[i];
    }
    ret = mam_api_add_ntru_pk(&_api, &new_pk);
    if(ret != RC_OK){
      printf("Error adding NTRU pubkey\n");
    }
    ret = mam_api_save(&_api, API_PATH, NULL, 0);
    if(ret != RC_OK){
      printf("Error saving  API object\n");
    }
    ret = mam_api_destroy(&_api);
    if(ret != RC_OK){
      printf("Error destroying  API object\n");
    }
    std::string result = extractAPIFromFile();
    return result;
  }
  
  std::string MAM::removeTrustedNTRUPubKey(const std::string &api, const std::string &ntru){
    saveAPIToFile(api);
    mam_api_t _api;
    mam_api_load(API_PATH, &_api, NULL, 0);
    mam_ntru_pk_t new_pk;
    trit_t key_trits[MAM_NTRU_PK_SIZE];
    trytes_to_trits((tryte_t *)ntru.c_str(), key_trits, ntru.length());
    for (unsigned int i = 0; i < MAM_NTRU_PK_SIZE; i++) {
      new_pk.key[i] = key_trits[i];
    }
    mam_api_remove_ntru_pk(&_api, &new_pk);
    mam_api_save(&_api, API_PATH, NULL, 0);
    mam_api_destroy(&_api);
    std::string result = extractAPIFromFile();
    return result;
  }
  
  std::string MAM::addTrustedChannel(const std::string &api, const std::string &channel){
    saveAPIToFile(api);
    mam_api_t _api;
    mam_api_load(API_PATH, &_api, NULL, 0);
    mam_api_add_trusted_channel_pk(&_api, (tryte_t *)channel.c_str());
    mam_api_save(&_api, API_PATH, NULL, 0);
    mam_api_destroy(&_api);
    std::string result = extractAPIFromFile();
    return result;
  }
  
  std::string MAM::addTrustedEndpoint(const std::string &api, const std::string &endpoint){
    saveAPIToFile(api);
    mam_api_t _api;
    mam_api_load(API_PATH, &_api, NULL, 0);
    mam_api_add_trusted_endpoint_pk(&_api, (tryte_t *)endpoint.c_str());
    mam_api_save(&_api, API_PATH, NULL, 0);
    mam_api_destroy(&_api);
    std::string result = extractAPIFromFile();
    return result;
  }
  
  std::vector<std::string> MAM::getVector(){
    std::vector<std::string> res;
    res.clear();
    return res;
  }
  
} // namespace Society2
