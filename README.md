# SOCIETY2 Messenger Prototype #
- - - -
## Visit us at [SOCIETY2](https://society2.com)

## How to build the project:

# Prerequesites:
1. yarn
2. nodejs
3. npm
4. rollup
5. bazel

# Process:
1. Fetch the resources recursively: `git clone --recurse-submodules https://gitlab.com/herrkpunkt/society2mvp.git`
2. Move into directory: `cd society2mvp`
3. Prepare directory: `yarn install`
4. Update and initialize emscripten: `yarn em:update && yarn em:init`
5. Build project: `yarn build`
6. Generate bundles: `yarn rollup`
