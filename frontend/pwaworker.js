//This file is part of Secrets on SOCIETY2.
//
//    Secrets on SOCIETY2 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Secrets on SOCIETY2 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Secrets on SOCIETY2.  If not, see <http://www.gnu.org/licenses/>.

const CACHE_NAME = 'secrets-cache-v0.1.1-RC2';
const urlsToCache = [
    './index.html',
    './favicon.ico',
    './assets/load-balancer.js-browser.js',
    './assets/iota.js-browser.js',
    './assets/crypto-js.js',
    './assets/society2-wasm.js',
    './assets/jsqr.js',
    './assets/bootstrap.min.css',
    './assets/handlebars.js',
    './fonts/fontawesome-webfont.woff2?v=4.7.0',
    './assets/jquery-3.5.1.min.js',
    './assets/bootstrap.min.js',
    './assets/moment.min.js',
    './assets/font-awesome.min.css',
    './assets/popper.min.js',
    './assets/qrcode.min.js',
    './assets/logo-full-square.png',
    './assets/logo-full.png',
    './assets/toastr.min.css',
    './assets/waves.min.js',
    './assets/toastr.min.js',
    './assets/waves.min.css',
    './assets/badge.png',
    './assets/templates.js',
    './assets/main.js',
    './assets/ui-scripts.js',
    './assets/s2.css'
];

self.addEventListener('install', function(event) {
  self.skipWaiting();
  event.waitUntil(
    caches.open(CACHE_NAME)
	  .then(function(cache) {
      return cache.addAll(urlsToCache);
    })
  );
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request).then(function(response) {
      return response || fetch(event.request);
    })
  );
});

self.addEventListener('activate', async (event) => {
  event.waitUntil(
    caches.keys().then(keys => Promise.all(
      keys.map(key => {
        if (!CACHE_NAME.includes(key)) {
          return caches.delete(key);
        }
      })
    )).then(async () => {

    })
  );
});
