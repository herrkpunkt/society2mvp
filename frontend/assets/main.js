//This file is part of Secrets on SOCIETY2.
//
//    Secrets on SOCIETY2 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Secrets on SOCIETY2 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Secrets on SOCIETY2.  If not, see <http://www.gnu.org/licenses/>.

import createModule from './society2-wasm.js';

var s2;
window.s2 = s2;
const mwm = 9
const depth = 1
const defaultNodes = ["https://altnodes.devnet.iota.org:443","https://nodes.devnet.iota.org:443", "https://tangle.society2.io:443"]
window.defaultNodes = defaultNodes;
const basedatastructure = '{"S2":{"Version": "", "Chats":{}, "GivenName": "New Identity", "Nodes": '+JSON.stringify(defaultNodes)+', "MWM": '+mwm+', "AutoRefreshInterval": 60}}'
window.basedatastructure = basedatastructure;
const basechatstructure = '{"GivenName": "New Chat", "Publisher": {"publisherAPI": "", "Messages": [], "PSK": ""}, "Subscribers": []}'
const basemessagestructure = '{"MessageID": "", "Content": "", "Timestamp": 0, "Attachment": "", "Purpose": "", "Reference": ""}'
const basesubscriberstructure = '{"SubscriberAPI": "", "Basechannel": "", "Messages": [], "Bundles": [], "GivenName": "", "WhoAmIName": "", "WhoAmIBio": "", "WhoAmIPic": "", "PSK": ""}'
const basewhoamistructure = '{"DisplayName": "", "Bio": "", "Pic": ""}'
const baseintromessagestructure = '{"Participants": []}'
const baseintromessageparticipantstructure = '{"GivenName": "", "Channel": "", "Endpoint": "", "NTRUPubkey": "", "PSK": ""}'
const event_working = new Event('working');
const event_finished_working = new Event('finished_working');
const event_message_sent = new Event('finished_message_sent');
const event_message_read = new Event('new_incoming_message');
const error_channel_is_own_pub = new Event('channel_is_own_pub');
const error_channel_is_known_sub = new Event('channel_is_known_sub');
const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ9';

//LOW LEVEL METHODS
//GENERATE SECRET FROM CHANNEL, PSK AND NAME
function generateSecret(channel, psk, name){
    var result = channel + psk + converter.asciiToTrytes(btoa(name));
    console.log("Secret generated: " + result);
    return result;
}
window.generateSecret = generateSecret;

//PARSE SECRET TO CHANNEL, PSK AND NAME
function parseSecret(secret){
    var result = {};
    result.Channel = secret.substring(0,81);
    result.PSK = secret.substring(81,162);
    result.name = atob(converter.trytesToAscii(secret.substring(162)));
    console.log("Secret parsed: " + "channel=" + result.Channel + " psk=" + result.PSK + " name=" + result.name);
    return result;
}
window.parseSecret = parseSecret;

//GENERATE TRYTES
function generateTrytes(size){
   var result           = '';
   var charactersLength = characters.length;
   for ( var i = 0; i < size; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}
window.generateTrytes = generateTrytes;

//GENERATE LOADBALANCER LIST
function createLBNodeList(){
  var ns = false; var nodeList = [];
  if(typeof(window.s2.S2.Nodes) != "undefined"){
    ns = window.s2.S2.Nodes;
  } else{window.s2.S2.Nodes = defaultNodes;saveToStorage();ns = defaultNodes};

  if (ns.length){
    for(var n in ns){
      if(n != ""){
        nodeList.push(
          {
              "provider": ns[n],
              "depth": 1,
              "mwm": window.s2.S2.MWM
          }
        );
      }
    }
  }
  return nodeList;
}
window.createLBNodeList = createLBNodeList;

//SEND BUNDLE GIVEN AS TRYTES ARRAY
async function sendBundle(trytes){
    const iota = lb.composeAPI({
            nodeWalkStrategy: new lb.RandomWalkStrategy(createLBNodeList()),
            successMode: lb.SuccessMode.next,
            failMode: lb.FailMode.all,
            timeoutMs: 5000,
            tryNodeCallback: (node) => {
            },
            failNodeCallback: (node, err) => {
            }
        });
    let tips = await iota.getTransactionsToApprove(depth);
    let attachedTrytes = await iota.attachToTangle(tips.trunkTransaction, tips.branchTransaction, window.s2.S2.MWM, trytes);
    await iota.storeAndBroadcast(attachedTrytes);
}

//GENERATE ARRAY FROM CPP VECTOR
function vecToArray(vec){
    var result = [];
    for (let i = 0; i < vec.size(); i++) {
        result.push(vec.get(i))
    }
    return result
}

//LOAD THE VAULT FROM LOCAL STORAGE
function loadFromStorage(){
    const ciphertext = localStorage.getItem('S2');
    const MAC = localStorage.getItem('S2HMAC');
    if(ciphertext != null && MAC != null){
	    const verifyMAC = CryptoJS.HmacSHA3(ciphertext, window.password);
	    if(MAC != verifyMAC){
	        window.toastr.error("Password Invalid!");
	    }
	    else{
	        const cleartext = CryptoJS.AES.decrypt(ciphertext, window.password).toString(CryptoJS.enc.Utf8);
	        s2 = JSON.parse(cleartext);
            window.s2 = s2;
	    }
    }
    else{}
}
window.loadFromStorage = loadFromStorage;

//SAVE THE VAULT TO LOCAL STORAGE
function saveToStorage(){
    s2 = window.s2;
    const jsoncontent = JSON.stringify(s2);
    const ciphertext = CryptoJS.AES.encrypt(jsoncontent, window.password).toString();
    const MAC = CryptoJS.HmacSHA3(ciphertext, window.password);
    localStorage.setItem('S2HMAC', MAC);
    localStorage.setItem('S2', ciphertext);
}
window.saveToStorage = saveToStorage;

//FETCH NEW BUNDLES FROM CHANNEL
async function fetchBundles(addr, old){
    const iota = lb.composeAPI({
            nodeWalkStrategy: new lb.RandomWalkStrategy(createLBNodeList()),
            successMode: lb.SuccessMode.next,
            failMode: lb.FailMode.all,
            timeoutMs: 5000,
            tryNodeCallback: (node) => {
            },
            failNodeCallback: (node, err) => {
            }
    });
    let txs = await iota.findTransactionObjects({ addresses: [addr] });
    let txs_after_deleting_olds = [];
    for(let tx of txs){
        let found = false;
        for(let bundle of old){
            if(tx.bundle == bundle){
                found = true;
            }
        }
        if(!found){
            txs_after_deleting_olds.push(tx);
        }
    }
    let tailtxs = [];
    let txs_without_tails = [];
    for(let tx of txs_after_deleting_olds){
        if(tx.currentIndex == 0){
            tailtxs.push(tx);
        }
        else{
            txs_without_tails.push(tx);
        }
    }
    let bundles = [];
    for(let tailtx of tailtxs){
        let bundle = [];
        bundle.push(transaction_converter.asTransactionTrytes(tailtx));
        let latesttrunk = tailtx.trunkTransaction;
        for(let i = 1; i < tailtx.lastIndex+1; i++){
            for(let tx of txs_without_tails){
                if(tx.hash == latesttrunk && tx.currentIndex == i){
                    latesttrunk = tx.trunkTransaction;
                    bundle.push(transaction_converter.asTransactionTrytes(tx));
                }
            }
        }
        if(bundle.length == transaction_converter.asTransactionObject(bundle[0]).lastIndex+1){
            bundles.push(bundle);
        }
    }
    let to_sort = [];
    for(let bundle of bundles){
        to_sort.push([bundle, transaction_converter.asTransactionObject(bundle[0]).timestamp]);
    }
    to_sort.sort((a,b) => a[1] - b[1]);
    let result = [];
    for(let bundle of to_sort){
        result.push(bundle[0]);
    }
    return result;
}
 window.fetchBundles = fetchBundles;

//HIGH LEVEL METHODS
//CREATE CHAT
async function createChat(name){
    window.dispatchEvent(event_working);
    loadFromStorage();
    var random = new Uint32Array(32);
    window.crypto.getRandomValues(random);
    let additional_entropy = random.join();
    let mam = await createModule();
    window.mam = mam;
    const psk = generateTrytes(81);
    var pub = await mam.MAM.generatePublisherAPI(additional_entropy, psk);
    await sendBundle(vecToArray(pub.get(1)));
    const ntru = await mam.MAM.getNTRUPubKey(pub.get(0).get(0));
    var keyload_msg = JSON.parse(basemessagestructure);
    keyload_msg.Purpose = "NTRUPUBKEY";
    keyload_msg.Content = ntru;
    const content = JSON.stringify(keyload_msg);
    pub = await mam.MAM.encrypt(content, pub.get(0).get(0), true, "", psk);
    for(let i = 1; i < pub.size(); i++) {
	await sendBundle(vecToArray(pub.get(i)))
    }
    let chat = JSON.parse(basechatstructure);
    chat.Publisher.publisherAPI = pub.get(0).get(0);
    chat.Publisher.PSK = psk;
    const channel = await mam.MAM.getWorkingChannelConfig(pub.get(0).get(0));
    chat.Publisher.ActiveChannel = channel.get(1).get(0);
    chat.ID = channel.get(1).get(0);
    chat.GivenName = name;
    s2.S2.Chats[chat.Publisher.ActiveChannel] = chat;
    window.s2 = s2;
    saveToStorage();
    window.dispatchEvent(event_finished_working);
    return chat;
}
window.createChat = createChat;

//REMOVE CHAT
async function removeChat(chatid){
    window.dispatchEvent(event_working);
    loadFromStorage();
    delete s2.S2.Chats[chatid];
    window.s2 = s2;
    saveToStorage();
    window.dispatchEvent(event_finished_working);
}
window.removeChat = removeChat;

//REMOVE SUBSCRIBER FROM CHAT
async function removeSubscriber(chatid, basechannel){
    window.dispatchEvent(event_working);
    loadFromStorage();
    let mam = await createModule();
    window.mam = mam;
    for(let subscriber of s2.S2.Chats[chatid].Subscribers){
        if(subscriber.Basechannel == basechannel){
            let ntru =  await mam.MAM.getTrustedNTRUPubKeys(subscriber.SubscriberAPI);
            for(var i=0;i<ntru.size();i++){
                s2.S2.Chats[chatid].Publisher.publisherAPI = await mam.MAM.removeTrustedNTRUPubKey(s2.S2.Chats[chatid].Publisher.publisherAPI, ntru.get(i));
            }
            const subscriberIndex = s2.S2.Chats[chatid].Subscribers.indexOf(subscriber);
            s2.S2.Chats[chatid].Subscribers.splice(subscriberIndex, 1);
            break;
        }
    }
    saveToStorage();
    window.s2 = s2;
    window.dispatchEvent(event_finished_working);
}
window.removeSubscriber = removeSubscriber;

//GET ALL CHANNELS FROM CHAT
async function getAllChannels(chatid){
    var result = [];
    var mam = await createModule();
    for(let subscriber of s2.S2.Chats[chatid].Subscribers){
        const channels_in_subscriber = await mam.MAM.getTrustedChannels(subscriber.SubscriberAPI);
        for(let i = 0; i < channels_in_subscriber.size(); i++){
            result.push(channels_in_subscriber.get(i));
        }
    }
    const own_channels = await mam.MAM.getTrustedChannels(s2.S2.Chats[chatid].Publisher.publisherAPI);
    for(let i = 0; i < own_channels.size(); i++){
        result.push(own_channels.get(i));
    }
    return result;
}
window.getAllChannels = getAllChannels;

//ADD SUBSCRIBER TO CHAT
async function addSubscriber(chatid, channel, name, psk){
    window.dispatchEvent(event_working);
    loadFromStorage();
    let mam = await createModule();
    window.mam = mam;
    const pub_channels = [];
    const sub_channels = [];
    for(let chat of Object.keys(s2.S2.Chats)){
        const pub_channels_in_chat = await mam.MAM.getChannels(s2.S2.Chats[chat].Publisher.publisherAPI);
        for(let i = 0; i < pub_channels_in_chat.size(); i++){
            pub_channels.push(pub_channels_in_chat.get(i));
        }
        for(let subscriber of s2.S2.Chats[chat].Subscribers){
            const sub_channels_in_subscriber = await mam.MAM.getTrustedChannels(subscriber.SubscriberAPI);
            for(let i = 0; i < sub_channels_in_subscriber.size(); i++){
                sub_channels.push(sub_channels_in_subscriber.get(i));
            }
        }
    }
    var found = false;
    for(let i = 0; i < pub_channels.length; i++){
        if(pub_channels[i] == channel){
            found = true;
            window.dispatchEvent(error_channel_is_own_pub);
            break;
        }
    }
    if(!found){
        for(let i = 0; i < sub_channels.length; i++){
            if(sub_channels[i] == channel){
                found = true;
                window.dispatchEvent(error_channel_is_known_sub);
                break;
            }
        }
    }
    if(!found){
        var subscriber = JSON.parse(basesubscriberstructure);
        subscriber["SubscriberAPI"] = await mam.MAM.generateReaderAPI(s2.S2.Chats[chatid].Publisher.publisherAPI, channel, psk);
        subscriber["Basechannel"] = channel;
        subscriber["GivenName"] = name;
	subscriber["PSK"] = psk;
        s2.S2.Chats[chatid].Subscribers.push(subscriber);
        window.s2 = s2;
        saveToStorage();
    }
    window.dispatchEvent(event_finished_working);
}
window.addSubscriber = addSubscriber;

//ACCEPT INTRODUCTION MESSAGE
async function acceptIntroduction(chatId, messageId, participantindexes){
    window.dispatchEvent(event_working);
    s2 = window.s2
    var mam = await createModule();
    window.mam = mam;
    let introMessage = "";
    let newSubscribers = [];
    for(let message of s2.S2.Chats[chatId].SortedMessages){
        if(message.MessageID == messageId){
            introMessage = message;
            console.log(introMessage);
            break;
        }
    }
    for(let index of participantindexes){
        const msgObj = JSON.parse(introMessage.Content).Participants[index];
        const channels_already_known = await getAllChannels(chatId);
        if(channels_already_known.includes(msgObj.Channel)){
            return;
        }
        var subscriber = JSON.parse(basesubscriberstructure);
        subscriber["SubscriberAPI"] = await mam.MAM.generateReaderAPI(s2.S2.Chats[chatId].Publisher.publisherAPI, msgObj.Channel, msgObj.PSK);
        subscriber["Basechannel"] = msgObj.Channel;
        subscriber["GivenName"] = msgObj.GivenName;
	subscriber["PSK"] = msgObj.PSK;
        subscriber["SubscriberAPI"] = await mam.MAM.addTrustedEndpoint(subscriber["SubscriberAPI"], msgObj.Endpoint);
        subscriber["SubscriberAPI"] = await mam.MAM.addTrustedNTRUPubKey(subscriber["SubscriberAPI"], msgObj.NTRUPubkey);
        s2.S2.Chats[chatId].Publisher.publisherAPI = await mam.MAM.addTrustedNTRUPubKey(s2.S2.Chats[chatId].Publisher.publisherAPI, msgObj.NTRUPubkey);
        s2.S2.Chats[chatId].Subscribers.push(subscriber);
        newSubscribers.push(msgObj.GivenName);
    }
    //for(let subscriberindex of Object.keys(s2.S2.Chats[chatId].Subscribers)){
    //    if(s2.S2.Chats[chatId].Subscribers[subscriberindex].Basechannel == subscriberBaseChannel){
    //        for(let messageindex of Object.keys(subscriber.Messages)){
    //            if(s2.S2.Chats[chatId].Subscribers[subscriberindex].Messages[messageindex].MessageID == messageId){
    //                s2.S2.Chats[chatId].Subscribers[subscriberindex].Messages[messageindex].Content = "(" + newSubscribers.join(', ') + " added)";
    //                break;
    //            }
    //        }
    //        break;
    //    }
    //}

    window.s2 = s2;
    //window.sortAllMessages();
    saveToStorage();
    window.toastr.success("New Participant Added: "+JSON.parse(introMessage.Content).Participants[participantindexes[0]].GivenName);
    window.openChat(chatId, false);
    window.dispatchEvent(event_finished_working);
}
window.acceptIntroduction = acceptIntroduction;

//SEND MESSAGE
async function sendMessage(chatid, msg, key = "", psk=""){
    window.dispatchEvent(event_working);
    let mam = await createModule();
    window.mam = mam;
    loadFromStorage();
    var chat = s2.S2.Chats[chatid];
    var publisherapi = chat["Publisher"]["publisherAPI"];
    const res = await mam.MAM.encrypt(JSON.stringify(msg), publisherapi, false, key, psk);
    for(let i = 1; i < res.size(); i++) {
        await sendBundle(vecToArray(res.get(i)));
    }
    s2.S2.Chats[chatid]["Publisher"]["publisherAPI"] = res.get(0).get(0);
    const channel = await mam.MAM.getWorkingChannelConfig(res.get(0).get(0));
    s2.S2.Chats[chatid]["Publisher"]["ActiveChannel"] = channel.get(1).get(0);
    const msgbundle = res.get(res.size()-1);
    msg["MessageID"] = transaction_converter.asTransactionObject(msgbundle.get(0)).bundle;
    msg["Timestamp"] = transaction_converter.asTransactionObject(msgbundle.get(0)).timestamp;
    s2.S2.Chats[chatid]["Publisher"]["Messages"].push(msg);
    saveToStorage();
    window.dispatchEvent(event_finished_working);
    window.dispatchEvent(event_message_sent);
}
window.sendMessage = sendMessage;

//REFRESH CHATS
async function refreshChats(chatids){
    window.dispatchEvent(event_working);
    var newMessageEvent = false
    let mam = await createModule();
    window.mam = mam;
    s2 = window.s2;
    //loadFromStorage();
    for(let chatid of chatids){
        var chat = s2.S2.Chats[chatid];
        for(let subscriber of chat.Subscribers){
            let index = -1;
            for(let i = 0; i < chat.Subscribers.length; i++){
                if(subscriber.Basechannel == chat.Subscribers[i].Basechannel){
                    index = i;
                    break;
                }
            }
            var channels = mam.MAM.getTrustedChannels(subscriber.SubscriberAPI);
            for(let i = channels.size()-1; i < channels.size(); i++){
                var bundles = await fetchBundles(channels.get(i), subscriber.Bundles);
                for(let trytes of bundles){
                    var vec = await mam.MAM.getVector();
	                if(trytes.length != transaction_converter.asTransactionObject(trytes[0]).lastIndex+1) continue;
	                for(let k = 0; k < trytes.length; k++){
                        vec.push_back(trytes[k]);
                    }
                    const read = await mam.MAM.decrypt(vec, subscriber.SubscriberAPI);
                    subscriber.SubscriberAPI = read.get(0);
                    var msgcontent = read.get(1);
                    if(msgcontent != ""){
                        var msg = JSON.parse(msgcontent);
                        msg.Timestamp = transaction_converter.asTransactionObject(trytes[0]).timestamp;
                        msg.MessageID = transaction_converter.asTransactionObject(trytes[0]).bundle;
                        subscriber.Bundles.push(msg.MessageID);
                        var newMessage = new CustomEvent('new_incoming_message', {
                            detail : {
                                message : msg,
                                chat: chatid,
                                from: subscriber
                            }
                        });
                        switch(msg.Purpose){
                            case "CONTENT":
                                subscriber.Messages.push(msg);
                                newMessageEvent = true;
                                break;
                            case "NTRUPUBKEY":
                                console.log("NTRU key received");
                                var NTRUPubKeyFromNewParticipant = msg.Content;
                                msg.Content = "(Participant Added)";
                                subscriber.Messages.push(msg);
                                const known_keys = await mam.MAM.getTrustedNTRUPubKeys(subscriber.SubscriberAPI);
                                if(known_keys.size() != 0){
                                    console.log("NTRU key already known");
                                    break;
                                }
                                subscriber.SubscriberAPI = await mam.MAM.addTrustedNTRUPubKey(subscriber.SubscriberAPI, NTRUPubKeyFromNewParticipant);
                                if(chat.Subscribers.length > 1){
                                    console.log("Starting introduction");
                                    var introcontent = JSON.parse(baseintromessagestructure);
                                    for(var subscriberToIntroduce of chat.Subscribers){
                                        if(subscriberToIntroduce.Basechannel == subscriber.Basechannel){
                                            console.log("Not introducing participant himself");
                                            continue;
                                        }
                                        var subscriberintrocontent = JSON.parse(baseintromessageparticipantstructure);
                                        subscriberintrocontent.GivenName = subscriberToIntroduce.GivenName;
                                        const channelsToSubscribe = mam.MAM.getTrustedChannels(subscriberToIntroduce.SubscriberAPI);
                                        if(channelsToSubscribe.size() == 0){
                                            console.log("No channel found");
                                            continue;
                                        }
                                        const endpointsToSubscribe = mam.MAM.getTrustedEndpoints(subscriberToIntroduce.SubscriberAPI);
                                        if(endpointsToSubscribe.size() == 0){
                                            console.log("No endpoint found");
                                            continue;
                                        }
                                        subscriberintrocontent.Channel = channelsToSubscribe.get(channelsToSubscribe.size()-1);
                                        subscriberintrocontent.Endpoint = endpointsToSubscribe.get(endpointsToSubscribe.size()-1);
					subscriberintrocontent.PSK = subscriberToIntroduce.PSK;
                                        const ntrus = mam.MAM.getTrustedNTRUPubKeys(subscriberToIntroduce.SubscriberAPI);
                                        if(ntrus.size() == 0){
                                            console.log("No NTRU key found");
                                            continue;
                                        }
                                        subscriberintrocontent.NTRUPubkey = ntrus.get(ntrus.size()-1);
                                        introcontent.Participants.push(subscriberintrocontent);
                                    }
                                    var intromsg = getParticipantIntroductionMessage(introcontent);
                                    console.log("first intro message: " + intromsg);
                                    sendMessage(chatid, intromsg, NTRUPubKeyFromNewParticipant);
                                    introcontent = JSON.parse(baseintromessagestructure);
                                    var subscriberintrocontent = JSON.parse(baseintromessageparticipantstructure);
                                    subscriberintrocontent.GivenName = subscriber.GivenName;
                                    channels = mam.MAM.getTrustedChannels(subscriber.SubscriberAPI);
                                    const endpoints = mam.MAM.getTrustedEndpoints(subscriber.SubscriberAPI);
                                    subscriberintrocontent.Channel = channels.get(channels.size()-1);
                                    subscriberintrocontent.Endpoint = endpoints.get(endpoints.size()-1);
                                    subscriberintrocontent.NTRUPubkey = NTRUPubKeyFromNewParticipant;
				    subscriberintrocontent.PSK = subscriber.PSK;
                                    introcontent.Participants.push(subscriberintrocontent);
                                    intromsg = getParticipantIntroductionMessage(introcontent);
                                    console.log("second intro message: " + intromsg);
                                    sendMessage(chatid, intromsg);
                                }
                                chat.Publisher.publisherAPI = await mam.MAM.addTrustedNTRUPubKey(chat.Publisher.publisherAPI, NTRUPubKeyFromNewParticipant);
                                break;
                            case "QUOTE":
                                subscriber.Messages.push(msg);
                                newMessageEvent = true;
                                break;
                            case "REACTION":
                                subscriber.Messages.push(msg);
                                newMessageEvent = true;
                                break;
                            case "WHOAMI":
                                break;
                            case "PARTICIPANTINTRODUCTION":
                                subscriber.Messages.push(msg);
                                newMessageEvent = true;
                                break;
                        }
                    }
	                else{
                        subscriber.Bundles.push(transaction_converter.asTransactionObject(trytes[0]).bundle);
                    }
                    chat.Subscribers[index] = subscriber;
                }
            }
            s2.S2.Chats[chatid] = chat;
        }
    }
    saveToStorage();
    //s2 = window.s2;
    if(newMessageEvent){window.dispatchEvent(newMessage);}
    window.dispatchEvent(event_finished_working);
}
window.refreshChats = refreshChats;

//Produce content message
function getContentMessage(msg, attachment){
    var result = JSON.parse(basemessagestructure);
    result["Content"] = msg;
    result["Attachment"] = attachment;
    result["Purpose"] = "CONTENT";
    return result;
}
window.getContentMessage = getContentMessage;

//Produce quote message
function getQuoteMessage(msg, attachment, ref){
    var result = JSON.parse(basemessagestructure);
    result["Purpose"] = "QUOTE";
    result["Content"] = msg;
    result["Attachment"] = attachment;
    result["Reference"] = ref;
    return result;
}
window.getQuoteMessage = getQuoteMessage;

//Produce reaction message
function getReactionMessage(reaction, ref){
    var result = JSON.parse(basemessagestructure);
    result["Purpose"] = "REACTION";
    result["Content"] = reaction;
    result["Reference"] = ref;
    return result;
}
window.getReactionMessage = getReactionMessage;

//Produce whoami message
function getWhoAmIMessage(dname, pic, bio){
    var result = JSON.parse(basemessagestructure);
    result["Purpose"] = "WHOAMI";
    var whoami = JSON.parse(basewhoamistructure);
    whoami.DisplayName = dname;
    whoami.Bio = bio;
    whoami.Pic = pic;
    result["Content"] = JSON.stringify(whoami);
    return result;
}
window.getWhoAmIMessage = getWhoAmIMessage;


//Produce participant introduction message
function getParticipantIntroductionMessage(introobject){
    var result = JSON.parse(basemessagestructure);
    result["Purpose"] = "PARTICIPANTINTRODUCTION";
    result["Content"] = JSON.stringify(introobject);
    return result;
}
window.getParticipantIntroductionMessage = getParticipantIntroductionMessage;

if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('pwaworker.js').then(function(registration) {
    registration.update();
  }).catch(function(error) {
    console.log('Registration failed with ' + error);
  });
};
