//This file is part of Secrets on SOCIETY2.
//
//    Secrets on SOCIETY2 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Secrets on SOCIETY2 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Secrets on SOCIETY2.  If not, see <http://www.gnu.org/licenses/>.

var worker = {};
var streamObject = {};
var video = {};

function decodeHTMLEntities(text) {
	var entities = [
		['amp', '&'],
		['apos', '\''],
		['#x27', '\''],
		['#x2F', '/'],
		['#39', '\''],
		['#47', '/'],
		['lt', '<'],
		['gt', '>'],
		['nbsp', ' '],
		['quot', '"'],
		['#123', '{'],
		['#125', '}']
	];

	for (var i = 0, max = entities.length; i < max; ++i)
	{
		text = text.replace(new RegExp('&'+entities[i][0]+';', 'g'), entities[i][1]);
	}

	return text;
}

function toClipboard(data) {
  $('body').append("<textarea id='temp-data'>"+data+"</textarea>");
  $("#temp-data").focus();
  $("#temp-data").select();

  try {
    var successful = document.execCommand('copy');
		navigator.clipboard.writeText(data);
    var msg = successful ? 'successful' : 'unsuccessful';
    if(msg === "successful") {
		toastr.success('Copied to Clipboard: ' + data);
	}
	$("#temp-data").remove();
  } catch (err) {
    toastr.error('Unable to copy to Clipboard: '+data);
  }
}

function replaceBraces(text) {
	text = text.replace(/%5B/g, "[");text = text.replace(/%5D/g, "]");text = text.replace(/%20/g, " ");
	text = text.replace(/\[\[/g, decodeHTMLEntities("&#123;&#123;"));
	text = text.replace(/\]\]/g, decodeHTMLEntities("&#125;&#125;"));
	return text;
}







function welcome(){
	$("#disclaimer-modal").modal('hide');
	var welcomeModal = Handlebars.compile("\{{> welcome-modal}}");
	$("#welcome-modal .modal-content").html(welcomeModal());
	$("#welcome-modal").modal();
}


function verifyPassword(){
	password = $('#login-modal input[type="password"]').val();
	loadFromStorage();
	if(typeof(s2) != "undefined"){
		$('#login-modal input[type="password"]').val("");
		$('.modal.show').modal('hide');
		setTimeout(function(){login();reloadAllChats();}, 400);
		toastr.success('Logged in as "'+s2.S2.GivenName+'"');
	}
}

function newVaultModal(){
	var identityModal = Handlebars.compile("\{{> identity-modal}}");
	$("#identity-modal .modal-content").html(identityModal(JSON.parse(basedatastructure).S2));
	$("#identity-modal").modal();
	$('[data-toggle="tooltip"]').tooltip();
	$("#identity-modal button").remove();
	$("#identity-modal .modal-content > .d-flex").append('<button type="button" onclick="newVault()" class="btn btn-primary waves-effect"><i class="fa fa-check mr-2"></i><span>Create Vault</span></button>');
	$("#identity-modal #myName").val("");
}

async function newVault(){
	temps2 = JSON.parse(basedatastructure);
	var tempname = $("#identity-modal #myName").val();
	if(tempname != ""){		temps2.S2.GivenName = tempname;	} else{toastr.error("An Identity Name is required!");return;}
	let p1 = $("#identity-modal #password1").val(); let p2 = $("#identity-modal #password2").val();
	if(p1 == p2 && p1 != ""){
		password = p1;
		window.s2 = temps2;
		login();
		saveToStorage();
		$("#identity-modal").modal('hide');
		toastr.success("New Identity Created");
	}
	else{toastr.error("Passwords empty or do not match!");}
}

function login(){
	sortAllMessages();
	var chatButtons = Handlebars.compile("\{{> chat-buttons}}");
	$("#chats").html(chatButtons(s2.S2));
	//var contactTabs = Handlebars.compile("\{{> contact-tabs}}");
	//$("#contact-tabs").html(contactTabs(s2.S2));
	//var userInfo = Handlebars.compile("\{{> user-info}}");
	//$("#user-info").html(userInfo(society2));
	$("#welcome-modal").modal('hide');
	$(".publisher-name").text(s2.S2.GivenName);
	autoRefreshStop();autoRefreshStart();
	$('[data-toggle="tooltip"]').tooltip();
}

function autoRefreshStart(){
	if(s2.S2.AutoRefreshInterval != "undefined" && s2.S2.AutoRefreshInterval != "" && s2.S2.AutoRefreshInterval > 0){
		refreshIntervalObject = setInterval(function(){
			  if(!document.hidden){reloadAllChats();}
			  autoRefreshStop();autoRefreshStart();
		}, (s2.S2.AutoRefreshInterval*1000));
	}
}


function autoRefreshStop(){
	try{clearInterval(refreshIntervalObject);}catch(e){};
}


function editIdentitySettings(){
	var identityModal = Handlebars.compile("\{{> identity-modal}}");
	$("#identity-modal .modal-content").html(identityModal(s2.S2));
	$("#identity-modal").modal();
}

function editSettings(){
	var settingsModal = Handlebars.compile("\{{> settings-modal}}");
	$("#settings-modal .modal-content").html(settingsModal(s2.S2));
	$("#settings-modal").modal();
}

function saveIdentitySettings(){
	try{
		streamObject.getTracks().forEach(function(track) {
			track.stop();
		});
	}catch(error){}
	s2.S2.GivenName = $("#identity-modal #myName").val();
	let p1 = $("#identity-modal #password1").val(); let p2 = $("#identity-modal #password2").val();
	if(p1 == p2){
		if(p1 != "" && p2 != ""){
			password = p1;
			toastr.warning("Your Password Has Changed !");
		}
		saveToStorage();
		login();
		$("#identity-modal").modal('hide');
		toastr.success("Identity Settings Saved");
	}
	else{
		toastr.error("Passwords do not match");
	}
}


function saveSettings(){
	try{
		streamObject.getTracks().forEach(function(track) {
			track.stop();
		});
	}catch(error){}

	s2.S2.Nodes[0] = $("#settings-modal #node-url").val();
	s2.S2.MWM = parseInt($("#settings-modal #mwm").val());
	s2.S2.AutoRefreshInterval = parseInt($("#settings-modal #autorefreshinterval").val());
  var newnodes = [];
	var nodename = "";
	$("#settings-modal .node").each(function(){
		nodename = $(this).val();
		if(nodename != ""){ newnodes.push(nodename); }
	});
	s2.S2.Nodes = newnodes;
	$("#settings-modal").modal('hide');
	toastr.success("Settings Saved");
	setTimeout(function(){
		saveToStorage()
		login();
	}, 500);
}


function viewInvitations(chatId, messageId){
	var subscriberIDs = " ";
	for(var s in s2.S2.Chats[chatId].Subscribers){
		subscriberIDs += s2.S2.Chats[chatId].Subscribers[s].Basechannel+" ";
	}

	var invitationsModal = Handlebars.compile("\{{> invitations-modal chatID='"+chatId+"' PublisherActiveChannel='"+s2.S2.Chats[chatId].Publisher.ActiveChannel+"' SubscriberIDs='"+subscriberIDs+"'}}");

	for(var m in s2.S2.Chats[chatId].SortedMessages){
		if(s2.S2.Chats[chatId].SortedMessages[m].MessageID == messageId){
			s2.S2.Chats[chatId].SortedMessages[m].Invitations = JSON.parse(s2.S2.Chats[chatId].SortedMessages[m].Content);
			$("#invitations-modal .modal-content").html(invitationsModal(s2.S2.Chats[chatId].SortedMessages[m]));
			$('[data-toggle="tooltip"]').tooltip();
			$("#invitations-modal").modal();
		}
	}
}

async function trustParticipant(chatId, messageId, participantindex){
	await acceptIntroduction(chatId, messageId, participantindexes);
	toastr.success('Participant Trusted');
}

function loadDemoData() {
	s2 = demoData;
	saveToStorage();
	login();
}


function openChat(chatID, showModal){
  //for(var m in s2.S2.Chats[chatID].SortedMessages){
	//	if(s2.S2.Chats[chatID].SortedMessages[m].Purpose = "PARTICIPANTINTRODUCTION"){
	//		var content = JSON.parse(s2.S2.Chats[chatID].SortedMessages[m].Content);
	//		var allchannels = await getAllChannels(chatID);
	//		var found = 0;
	//		for(var p in content.Participants){
	//			if(includes(allchannels, content.Participants[p].Channel)){
	//				found++;
	//			}
	//		}
	//		if(found >= allChannels.length){
	//			s2.S2.Chats[chatID].SortedMessages.splice(m);
	//		}
	//	}
	//}

	var chatTemplate = Handlebars.compile("\{{> chat}}");
	$("#chat-modal .modal-content").html(chatTemplate(s2.S2.Chats[chatID]));
	$("#chat-modal").attr("data-chat-id", chatID);

  $(".message").off("click");
	$(".message").on("click", function(){
		openReactions(chatID, $(this).attr("data-message-id"));
	});

  $('#chat-modal [data-reference-id]').each(function(){
		var el = $(this);
		var ref = $(el).attr('data-reference-id');
		var responseText = $(el).text();
		for(m in s2.S2.Chats[chatID].SortedMessages){
			if(s2.S2.Chats[chatID].SortedMessages[m].MessageID == ref){
				var quotedMessage = s2.S2.Chats[chatID].SortedMessages[m];
				quotedMessage.ChatID = chatID;quotedMessage.OriginalText = responseText;quotedMessage.OriginalAuthor = s2.S2.Chats[chatID].SortedMessages[m].GivenName;
				var quoteTemplate = Handlebars.compile("\{{> quote-response}}");
				$(el).html(quoteTemplate(quotedMessage));
			}
		}
	});

	if(showModal){$("#chat-modal").modal();}

  $('[data-toggle="tooltip"]').tooltip();
	$("#chat-modal textarea").on("keypress", function(event){
			if (event.keyCode === 13 && event.shiftKey) {
	        event.preventDefault();
	        $("#chat-modal textarea + button").click();
	    }
	});
}



async function reloadAllChats(){
	if(typeof(s2) != "undefined" && typeof(s2.S2) != "undefined" && typeof(s2.S2.Chats) != "undefined" ){
		chatids = [];
	  for(let chatid in s2.S2.Chats){
			chatids.push(chatid);
		}
		if(chatids.length && typeof(s2) != "undefined"){
			toastr.warning("Synchronizing All Chats");
			await refreshChats(chatids);
			login();
			toastr.success("All Chats Synchronized");
			if($('#chat-modal.show').length){
				 var tmp = $("#chat-modal textarea").val();
	       let chatid = $('#chat-modal.show').attr("data-chat-id");
				 openChat(chatid, false);
				 $("#chat-modal textarea").val(tmp);
				 //$("#chat-modal textarea").focus();
			}
		}
	}
}

async function reloadChat(chatID){
	var tmp = $("#chat-modal textarea").val();
	$("body").addClass("loading");
	toastr.warning("Synchronizing Chat With Tangle");
	await refreshChats([chatID]);
	login();
	openChat(chatID, false);
	$("#chat-modal textarea").val(tmp);
	//$("#chat-modal textarea").focus();
	$("body").removeClass("loading");
	toastr.success("Chat Synchronized");
}


async function newChat(){
	var newname = $("#new-chat-name").val();
	if(newname != ""){
		$("#new-chat-modal").modal("hide");
		$("#new-chat-name").val("");
		$("body").addClass("loading");
		toastr.warning("Creating New Chat,\n\rPlease wait...");
		var c = await createChat(newname);
		$("body").removeClass("loading");
		toastr.success("New Chat Created");
		login();
		openChat(c.ID, true);
	}
	else{
		toastr.error("Chat name must not be empty!");
	}
}

async function deleteChat(chatID){
	delete s2.S2.Chats[chatID];
	saveToStorage();
	$(".modal.show").modal('hide');
	login();
	toastr.success("Chat Deleted");
}

function addParticipant(chatID){
	$('.modal').modal('hide');
	var addParticipantModal = Handlebars.compile("\{{> add-participant-modal}}");
	$("#add-participant-modal .modal-content").html(addParticipantModal(s2.S2.Chats[chatID]));
	$('[data-toggle="tooltip"]').tooltip();
	$('#add-participant-carousel').carousel({
	  interval: false
	});
	var qrcode = new QRCode("active-channel-qr", {
		text: s2.S2.Chats[chatID].Publisher.ActiveChannel,
		width: 400,
		height: 400,
		colorDark : "#000000",
		colorLight : "#ffffff",
		correctLevel : QRCode.CorrectLevel.H
	});
	$("#add-participant-modal").modal();
}

async function newSubscriber(chatID){
	var newsubscriberchannel = $("#newsubscriberchannel").val();
	var newsubscribername = $("#newsubscribername").val();
	var chars = /^[ABCDEFGHIJKLMNOPQRSTUVWXYZ9]+$/;
	if(newsubscriberchannel.match(chars) && newsubscriberchannel.length == 81 && newsubscriberchannel != s2.S2.Chats[chatID].Publisher.ActiveChannel) {
		if(newsubscriberchannel != "" && newsubscribername != ""){
			toastr.warning("Adding Participant,\n\rPlease wait...");
			await addSubscriber(chatID, newsubscriberchannel, newsubscribername);
			toastr.success("New Participant Added.");
			await reloadChat(chatID);
			loadFromStorage();
			login();
			if($("#chat-modal.show").length){
				openChat(chatID, false);
				editParticipants(chatID, false);
			}
			if($("#add-participant-modal.show").length){
				$("#add-participant-carousel").carousel("next");
			}
		}
	}
  else { toastr.error("Invalid Participant Secret"); }
}

function editParticipants(chatID, showModal){
	var participantsModal = Handlebars.compile("\{{> participants-modal}}");
	$("#participants-modal .modal-content").html(participantsModal(s2.S2.Chats[chatID]));
	$('[data-toggle="tooltip"]').tooltip();
	if(showModal){$("#participants-modal").modal();}
}

function saveParticipants(chatID){
	$("#participants-modal").modal('hide');
	setTimeout(function(){
		saveToStorage()
		login();
	}, 500);
	openChat(chatID, false);
}


async function deleteSubscriber(chatchannel, basechannel){
	 $("body").addClass("loading");
	 await removeSubscriber(chatchannel, basechannel);
	 $("body").removeClass("loading");
	 openChat(chatchannel, false);
	 editParticipants(chatchannel, false);
	 toastr.success("Participant Removed");
}

async function newMessage(chatID){
	var quote = false;
	var quoteMessageId  = "";
	var msg = $("#chat-modal textarea").val();
	if(msg != ""){
    quoteMessageId = $('#chat-modal .input .quoted-message [data-message-id]').attr('data-message-id');
		if(quoteMessageId){quote=true;}
		if(s2.S2.Chats[chatID].Subscribers.length){
			$("body").addClass("loading");
			toastr.success("Sending Message To Tangle...");
			//await sendMessage(chatID, getContentMessage(msg,""));
			try{
				if(!quote){
					await sendMessage(chatID, getContentMessage(msg,""));
				}else{
					await sendMessage(chatID, getQuoteMessage(msg,"",quoteMessageId));
				}

			}catch(e){console.log(e);console.log("Sending Failed");toastr.error("Sending Failed")};

			$("body").removeClass("loading");
			toastr.success("Message Sent");
			login();
			openChat(chatID, false);
		}
		else{
			toastr.error("You must add a participant first!");
		}
	}
}

async function newReply(chatID, messageID){
	for(message in s2.S2.Chats[chatID].SortedMessages){
		if(s2.S2.Chats[chatID].SortedMessages[message].MessageID == messageID){
			var quotedContent = escapeHtml(s2.S2.Chats[chatID].SortedMessages[message].Content.substring(0,100));
			quotedContent = '<b>'+s2.S2.Chats[chatID].SortedMessages[message].GivenName+':</b><br><div data-message-id="'+messageID+'">'+quotedContent+'</div>';
			$('#reactions-modal').modal("hide");
			$('#chat-modal .quoted-message').html('<div class="content waves-effect">'+quotedContent+'<i class="fa fa-close waves-effect" title="Remove Quote" onclick="$(\'.quoted-message .content\').remove();"></i></div>');
			$('#chat-modal .input textarea').focus();
		}
	}
}

function openReactions(chatID, messageID){
	$("#reactions-modal").attr("data-message-id", messageID);
	$("#reactions-modal").attr("data-chat-id", chatID);
	var reactions = Handlebars.compile("\{{> reactions-modal}}");
	for(var m in s2.S2.Chats[chatID].SortedMessages){
		if(s2.S2.Chats[chatID].SortedMessages[m].MessageID == messageID){
			$("#reactions-modal .modal-content").html(reactions(s2.S2.Chats[chatID].SortedMessages[m]));
			$("#reactions-modal").modal();
		}
	}
}

async function newReaction(reaction, chatID, messageID){
	for(var message in s2.S2.Chats[chatID].SortedMessages){
		if(s2.S2.Chats[chatID].SortedMessages[message].MessageID == messageID){
        console.log(s2.S2.Chats[chatID].SortedMessages);
				$("body").addClass("loading");
				toastr.success("Sending Reaction To Tangle...");
				try{
					await sendMessage(chatID, getReactionMessage(reaction, messageID));
				}catch(e){console.log(e);console.log("Sending Failed");}
				$("body").removeClass("loading");
				toastr.success("Reaction Sent");
				login();
				openChat(chatID, false);
				$('#reactions-modal').modal('hide');
				break;
		}
	}
}


function editChat(chatID, showModal){
	var chatSettingsModal = Handlebars.compile("\{{> chat-settings-modal}}");
	$("#chat-settings-modal .modal-content").html(chatSettingsModal(s2.S2.Chats[chatID]));
	$('[data-toggle="tooltip"]').tooltip();
	var qrcode = new QRCode("active-channel-qr", {
		text: s2.S2.Chats[chatID].Publisher.ActiveChannel,
		width: 400,
		height: 400,
		colorDark : "#000000",
		colorLight : "#ffffff",
		correctLevel : QRCode.CorrectLevel.H
	});
	$('#chat-settings-modal').on('hidden.bs.modal', function (e) {
		try{
			video.pause();
			streamObject.getTracks().forEach(function(track) {
				track.stop();
			});
		} catch(e){}
	});
	if(showModal){$("#chat-settings-modal").modal();}
}

function saveChatSettings(chatID){
	$("#chat-settings-modal").modal('hide');
	saveToStorage();
	login();
	openChat(chatID, false);
}




function exportData() {
	  var filename = s2.S2.GivenName+".s2";
		var data = {};
		data.S2 = localStorage.S2;
		data.S2HMAC = localStorage.S2HMAC;
		var data_json = JSON.stringify(data);
    var blob = new Blob([data_json], {type: 'text'});
    if(window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveBlob(blob, filename);
    }
    else{
        var elem = window.document.createElement('a');
        elem.href = window.URL.createObjectURL(blob);
        elem.download = filename;
        document.body.appendChild(elem);
        elem.click();
        document.body.removeChild(elem);
    }
}


function importData() {
  var file = $('#uploadProfile')[0].files[0];
  var reader = new FileReader();

  // Closure to capture the file information.
  reader.onload = (function(theFile) {
      return function(e) {
				try{var data = JSON.parse(e.target.result);} catch(error){toastr.error("Cannot Read Identity Package");}
				if(typeof(data) != "" && typeof(data.S2) != "undefined" && typeof(data.S2HMAC) != "undefined"){
					localStorage.S2 = data.S2;
					localStorage.S2HMAC = data.S2HMAC;
					window.location.reload();
				}
				else{
					toastr.error("Identity Data Incomplete");
				}
      };
    })(file);
    reader.readAsText(file);
}



async function scanQRCode(chatID){
	  video = document.createElement("video");
    var canvasElement = document.getElementById("canvas");
    var canvas = canvasElement.getContext("2d");
    var loadingMessage = document.getElementById("loadingMessage");
    var outputContainer = document.getElementById("output");
    var outputMessage = document.getElementById("outputMessage");
    var outputData = document.getElementById("outputData");
		var finished = false;
		loadingMessage.hidden = false;
		outputContainer.hidden = false;

    function drawLine(begin, end, color) {
      canvas.beginPath();
      canvas.moveTo(begin.x, begin.y);
      canvas.lineTo(end.x, end.y);
      canvas.lineWidth = 4;
      canvas.strokeStyle = color;
      canvas.stroke();
    }

    // Use facingMode: environment to attemt to get the front camera on phones
    navigator.mediaDevices.getUserMedia({ video: { facingMode: "environment" } }).then(function(stream) {
			streamObject = stream;
      video.srcObject = stream;
      video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
      video.play();
			console.log(video);
      requestAnimationFrame(tick);
    });

    function tick() {
      loadingMessage.innerText = "⌛ Loading video..."
      if (video.readyState === video.HAVE_ENOUGH_DATA) {
        loadingMessage.hidden = true;
        canvasElement.hidden = false;
        outputContainer.hidden = false;

        canvasElement.height = video.videoHeight;
        canvasElement.width = video.videoWidth;
        canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);
        var imageData = canvas.getImageData(0, 0, canvasElement.width, canvasElement.height);
        var code = jsQR(imageData.data, imageData.width, imageData.height, {
          inversionAttempts: "dontInvert",
        });
        if (code) {
					finished = true;
					video.pause();
          drawLine(code.location.topLeftCorner, code.location.topRightCorner, "#FF3B58");
          drawLine(code.location.topRightCorner, code.location.bottomRightCorner, "#FF3B58");
          drawLine(code.location.bottomRightCorner, code.location.bottomLeftCorner, "#FF3B58");
          drawLine(code.location.bottomLeftCorner, code.location.topLeftCorner, "#FF3B58");
          outputMessage.hidden = true;
          outputData.parentElement.hidden = false;
          outputData.innerText = code.data;
					streamObject.getTracks().forEach(function(track) {
					  track.stop();
					});
					toastr.success("Secret found in QR Code");
					setTimeout(function(){
						loadingMessage.hidden = true;
		        canvasElement.hidden = true;
		        outputContainer.hidden = true;
						outputData.parentElement.hidden = true;
						$("#newsubscriberchannel").val(code.data);
					},400);

        } else {
          outputMessage.hidden = false;
          outputData.parentElement.hidden = true;
        }
      }
			if(!finished){
				requestAnimationFrame(tick);
			}

    }
}

function sendEmailInvite(chatID){
	window.location.href = "mailto:?subject=Society2 Chat Invite&body=This is my secret on Society2:%0D%0A"+s2.S2.Chats[chatID].ID;
}


function sortAllMessages(){
	try{
		for(chat in s2.S2.Chats){
			let allMessages = [];
			for(pubMessage in s2.S2.Chats[chat].Publisher.Messages){
				s2.S2.Chats[chat].Publisher.Messages[pubMessage].Direction = "outgoing";
				s2.S2.Chats[chat].Publisher.Messages[pubMessage].GivenName = s2.S2.GivenName+" (me)";
			}
			allMessages = allMessages.concat(s2.S2.Chats[chat].Publisher.Messages);
			for(subscriber in s2.S2.Chats[chat].Subscribers){
				for(recMessage in s2.S2.Chats[chat].Subscribers[subscriber].Messages){
					s2.S2.Chats[chat].Subscribers[subscriber].Messages[recMessage].Direction = "incoming";
					s2.S2.Chats[chat].Subscribers[subscriber].Messages[recMessage].GivenName = s2.S2.Chats[chat].Subscribers[subscriber].GivenName
				}
				allMessages = allMessages.concat(s2.S2.Chats[chat].Subscribers[subscriber].Messages);
			}

			function sortMessagesByTimeStamp(a, b) {
			  let result = 0;
			  if (a.Timestamp > b.Timestamp) {
				result = -1;
			  } else if (a.Timestamp < b.Timestamp) {
				result = 1;
			  }
			  return result;
			}
			allMessages.sort(sortMessagesByTimeStamp);
			window.s2.S2.Chats[chat].LatestMessage = allMessages[0] || "";
			window.s2.S2.Chats[chat].SortedMessages = allMessages;
			window.s2.S2.Chats[chat].ID = chat;
		}

		function sortChatsByLatestMessage(a, b) {
		  let result = 0;
			if(a.SortedMessages.length == 0){result = -1}
			else if(b.SortedMessages.length == 0){result = 1}
		  else if (a.SortedMessages[a.SortedMessages.length-1].Timestamp > b.SortedMessages[b.SortedMessages.length-1].Timestamp) {
				result = -1;
		  }
			else if (a.SortedMessages[0].Timestamp < b.SortedMessages[0].Timestamp) {
				result = 1;
		  }
		  return result;
		}

		var sortedChats = Object.values(window.s2.S2.Chats);
		sortedChats.sort(sortChatsByLatestMessage);
		var sChats = [];
		for(chat in sortedChats) {
			sChats.push(sortedChats[chat].ID);
		}

		window.s2.S2.SortedChats = sChats;
	} catch(e){}

}


var entityMap = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#39;',
  '/': '&#x2F;',
  '`': '&#x60;',
  '=': '&#x3D;'
};

function escapeHtml (string) {
  return String(string).replace(/[&<>"'`=\/]/g, function (s) {
    return entityMap[s];
  });
}
