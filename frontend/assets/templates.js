//This file is part of Secrets on SOCIETY2.
//
//    Secrets on SOCIETY2 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Secrets on SOCIETY2 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Secrets on SOCIETY2.  If not, see <http://www.gnu.org/licenses/>.

Handlebars.registerHelper('eq', function (a, b) {
	return a == b;
});
Handlebars.registerHelper('gt', function (a, b) {
	return a > b;
});
Handlebars.registerHelper('lt', function (a, b) {
	return a < b;
});
Handlebars.registerHelper('add', function (a, b) {
	return a + b;
});
Handlebars.registerHelper('substract', function (a, b) {
	return a + b;
});
Handlebars.registerHelper('and', function (a, b) {
	return a && b;
});
Handlebars.registerHelper('or', function (a, b) {
	return a || b;
});
Handlebars.registerHelper('first', function (a) {
	if(a) {return a[0];}
});
Handlebars.registerHelper('last', function (a) {
	if(a) {return a[a.length-1];}
});
Handlebars.registerHelper('includes', function (a, b) {
	if(a && b) {return a.includes(b);}
});
Handlebars.registerHelper('rtrim', function (a, b) {
	if(a && b) {return a.trimRight(b);}
});
Handlebars.registerHelper('ltrim', function (a, b) {
	if(a && b) {return a.trimLeft(b);}
});
Handlebars.registerHelper('count', function (a) {
	if(a) {return a.length;}
});
Handlebars.registerHelper('decode', function (a) {
	if(a) {return JSON.parse(a);}
});
Handlebars.registerHelper('date', function (a) {
	var date = new Date(parseInt(a));
	var datetimestring = date.toLocaleString('default', { month: 'long' })+" "+date.getDate()+" "+date.getFullYear()+" "+date.getHours()+":"+date.getMinutes();
	return datetimestring;
});
Handlebars.registerHelper('truncate', function (text, limit, ellipsis) {
	if(text.length > limit){var newText = text.substr(0,limit) + (ellipsis ? ellipsis : '');}
	else{var newText = text}
	return newText;
});

Handlebars.registerHelper('moment', function(context, block) {
  if (window.moment) {
    var f = block.hash.format || "MMM Do, YYYY";
    return moment(new Date(context)).format(f);
  }else{
    return context;   //  moment plugin not available. return data as is.
  };
});

Handlebars.registerHelper('relativeMoment', function(context, block) {
  if (window.moment) {
    var f = block.hash.format || "MMM Do, YYYY";
    return moment(new Date(context), f).calendar();
  }else{
    return context;   //  moment plugin not available. return data as is.
  };
});


Handlebars.registerPartial(
	"welcome-modal",
	'\{{#if .}}'
	+'<button onclick="login()" class="btn d-flex flex-column flex-center secondary-color white-text z-depth-1 hoverable waves-effect mb-1 mx-auto">'
	+'<i class="fa fa-user fa-3x m-0 pb-1"></i>'
	+'<span>Use Current Identity</span>'
	+'<span>"\{{me.name}}"</span>'
	+'</button>'
	+'\{{/if}}'
	+'<div class="d-flex flex-column">'
	  +'<div class="mx-auto pt-2"><img style="width:100%;max-width:300px;height:auto;" src="./assets/logo-full.png" alt="SOCIETY 2 Logo"></div>'
		+'<div class="d-flex my-3 px-3 pt-3">'
			+'<button onclick="newVaultModal();" class="btn d-flex flex-column justify-content-center align-items-center z-depth-1 hoverable w-100 waves-effect">'
				+'<i class="fa fa-user fa-3x m-0 pb-1"></i>'
					+'<span class="font-weight-bold">Create Vault</span>'
			+'</button>'
			+'<button onclick="$(\'#uploadProfile\').click();" class="btn d-flex flex-column justify-content-center align-items-center  z-depth-1 hoverable w-100 waves-effect">'
				+'<i class="fa fa-upload fa-3x m-0 pb-1"></i>'
				+'<span class="font-weight-bold">Import Vault</span>'
			+'</button>'
		+'</div>'
	+'</div>'
);

Handlebars.registerPartial(
	"user-info",
	'<div class="pr-1half ml-auto">Logged in as: </div>'
	+'<div class="d-flex flex-center waves-effect rounded rgba-black-slight pl-1 z-depth-1" onclick="editIdentity()">'
	+'<div class="name">\{{me.name}}</div>'
	+'<div class="profile-image white rounded lazy ml-1" style="margin-right:0;" data-background-image="\{{me.image}}"></div>'
	+'</div>'
);

Handlebars.registerPartial(
	'identity-modal',
	'<div class="d-flex flex-column p-5">'
		+'<div class="d-flex mb-5"><i class="fa fa-5x fa-lock mr-4 ml-auto"></i><div class="h4 my-auto mr-auto">Vault Settings</div></div>'
		+'<h5 class="">Vault Name <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Required"></i></h5>'
		+'<input id="myName" type="text" class="w-100 mb-4" placeholder="New Identity" required value="\{{GivenName}}">'
		+'<h5 class="">Vault Password <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="A password is required"></i></h5>'
		+'<div class="d-flex flex-column flex-md-row"><input id="password1" type="password" class="w-40" placeholder="Password"><input id="password2" type="password" class="w-40 ml-0 ml-md-2" placeholder="Repeat"></div>'
		+'<div class="btn-group mt-5">'
			+'<button type="button" onclick="saveIdentitySettings()" class="btn btn-success waves-effect">'
					+'<i class="fa fa-check mr-2"></i><span>Save & Close</span></button>'
			+'<button type="button" class="btn btn-danger" onclick="if (confirm(\'This will delete your local vault and all chat data!\\nThis action cannot be undone!\')) { localStorage.clear();location.reload() }">'
					+'<i class="fa fa-times mr-2"></i><span>Delete Vault</span></button>'
		+'</div>'
		+'<div class="btn-group mt-3">'
			+'<button type="button" onclick="exportData()" class="btn btn-secondary waves-effect">'
					+'<i class="fa fa-download mr-2"></i><span>Export Vault</span></button>'
		+'</div>'
	+'</div>'
);

Handlebars.registerPartial(
	'settings-modal',
	'<div class="d-flex flex-column p-5">'
	  +'<div class="d-flex mb-5"><i class="fa fa-5x fa-sitemap mr-4 ml-auto"></i><div class="h4 my-auto mr-auto">Network Settings</div></div>'
	  +'<h5 class="">Active Nodes</h5>'
		+'<div id="node-template" class="d-none"><div class="d-flex"><input type="text" class="node w-100"></input><i onclick="$(this).parent().remove();" class="fa fa-minus-circle waves-effect my-auto ml-2 primary-text waves-effect"></i></div></div>'
    +'<div id="nodes-wrapper">'
			+'\{{#each Nodes as |node|}}'
				+'<div class="d-flex"><input disabled="true" type="text" class="node w-100" value="\{{node}}"></input><i onclick="$(this).parent().remove();" class="fa fa-minus-circle my-auto ml-2 primary-text waves-effect" title="Remove Node"></i></div>'
			+'\{{/each}}'
		+'</div>'
		+'<div class="d-flex mt-2 mb-4">'
			+'<button onclick="$(\'#nodes-wrapper\').append($(\'#node-template\').html());" class="btn btn-sm btn-info mt-1"><i class="fa fa-plus-circle mr-2"></i>Add Node</button>'
			+'<button onclick="s2.S2.Nodes=defaultNodes;saveToStorage();editSettings();"class="btn btn-sm btn-warning mt-1 ml-2 mr-auto"><i class="fa fa-refresh mr-2"></i>Load Default Nodes</button>'
		+'</div>'
		+'<h5 class="">Auto-Refresh Interval <span style="font-size:11px;">in Seconds, 0 = disable</span></h5>'
		+'<input id="autorefreshinterval" type="text" class="w-100 mb-4" placeholder="Node URL" value="\{{AutoRefreshInterval}}">'
		+'<h5 class="">Minimum Weight Magnitude</h5>'
		+'<input id="mwm" type="text" class="w-100 mb-4" placeholder="Node URL" value="\{{MWM}}">'
		+'<div class="btn-group mt-5">'
			+'<button type="button" onclick="saveSettings()" class="btn btn-success waves-effect">'
					+'<i class="fa fa-check mr-2"></i><span>Save Settings</span></button>'
		+'</div>'
	+'</div>'
);



Handlebars.registerPartial(
	"chat-buttons",
	'\{{#each SortedChats}} \{{#with (lookup ../Chats .)}}'
	+'<div class="chat-button waves-effect" onclick="openChat($(this).attr(\'data-chatid\'), true);" data-chatid="{{ID}}">'
		+'<div class="">'
			+'<div class="d-flex flex-column">'
				+'<div class="d-flex flex-column flex-md-row align-item-center">'
					+'<span class="name">\{{GivenName}}</span>'
					+'<span class="last-message-date ml-0 ml-md-auto">\{{#with (first SortedMessages)}}\{{relativeMoment Timestamp format="MMM DD YYYY - HH:mm"}}\{{/with}}</span>'
				+'</div>'
			+'</div>'
			+'<div class="d-flex">'
				+'<div class="last-message mr-auto">\{{#with (first SortedMessages)}}\{{GivenName}}: {{#unless (eq Purpose "PARTICIPANTINTRODUCTION")}} {{truncate Content 50 "..."}} {{else}} Participants added {{/unless}}\{{/with}}</div>'
				+'<div class="waves-effect d-none d-md-block mr-2" onclick="event.stopPropagation();addParticipant(\'{{ID}}\');"><i class="fa fa-plus" data-toggle="tooltip" data-placement="right" title="Add Participant to this Chat"></i></div>'
				+'<div class="waves-effect d-none d-md-block mr-2" onclick="event.stopPropagation();editParticipants(\'{{ID}}\', true);"><i class="fa fa-users" data-toggle="tooltip" data-placement="right" title="Manage Participants"></i></div>'
				+'<div class="waves-effect d-none d-md-block" onclick="event.stopPropagation();editChat(\'{{ID}}\', true);"><i class="fa fa-gear" data-toggle="tooltip" data-placement="right" title="Edit Chat Settings"></i></div>'
			+'</div>'
		+'</div>'

	+'</div>'
	+'\{{/with}}\{{/each}}'
	+'<div class="d-flex flex-column flex-md-row">'
		+'<div class="d-flex px-5 py-2 mt-2 waves-effect" onclick="$(\'#new-chat-modal\').modal();">'
			+'<i class="fa fa-plus fa-2x"></i>'
			+'<span class="my-auto ml-3">Create New Chat</span>'
		+'</div>'
		+'<div class="d-flex px-5 py-2 mt-2 waves-effect" onclick="autoRefreshStop();reloadAllChats()">'
			+'<i class="fa fa-refresh fa-2x"></i>'
			+'<span class="my-auto ml-3">Refresh All Chats</span>'
		+'</div>'
	+'</div>'
);

Handlebars.registerPartial(
	"chat",
	'<div class="">'
	  +'<div class="chat-controls">'
			+'<div class="waves-effect" onclick="$(\'#chat-modal\').modal(\'hide\');" data-toggle="tooltip" data-placement="left" title="Close Chat"><i class="fa fa-times m-0"></i></div>'
			+'<div class="waves-effect" onclick="reloadChat(\'{{ID}}\');"><i class="fa fa-refresh m-0" data-toggle="tooltip" data-placement="left" title="Download new messages"></i></div>'
			+'<div class="waves-effect" onclick="addParticipant(\'{{ID}}\')" data-toggle="tooltip" data-placement="left" title="Add Participants"><i class="fa fa-plus m-0"></i></div>'
			+'<div class="waves-effect" onclick="editParticipants(\'{{ID}}\', true)" data-toggle="tooltip" data-placement="left" title="Manage Participants"><i class="fa fa-users m-0"></i></div>'
			+'<div class="waves-effect" onclick="editChat(\'{{ID}}\', true)" data-toggle="tooltip" data-placement="left" title="Edit Chat Settings"><i class="fa fa-gear m-0"></i></div>'
		+'</div>'
		+'<div class="chat-title">'
			+'<div class="chat-name">\{{GivenName}}</div>'
			+'<div class="chat-subscribers">'
				+'{{#unless Subscribers}}- no participants yet -<div></div>{{/unless}}'
				+ '\{{#each Subscribers as |subscriber|}}'
					+  '{{GivenName}}{{#if (lt (add @index 1) (count @root.Subscribers)) }},{{/if}} '
				+'\{{/each}}'
				+'\{{#if (lt (count Subscribers) 1)}}'
					+'<div><button class="btn btn-sm btn-warning mt-2" onclick="editParticipants(\'{{ID}}\', true)"><i class="fa fa-plus mr-2"></i>Add Participants</button></div></div>'
				+'{{/if}}'
			+'</div>'
		+'</div>'
		+'<div class="messages-wrapper">'
			+'\{{#each SortedMessages as |m| }}'
				+'{{> message m ID=../ID}}'
			+'\{{/each}}'
		+'</div>'
		+'<div class="input d-flex flex-column mt-1">'
		  +'<div class="quoted-message"></div>'
			+'<div class="d-flex">'
				+'<textarea class="w-100" oninput="$(\'#chat-modal .messages-wrapper\').scrollTop($(\'#chat-modal .messages-wrapper\')[0].scrollHeight);"></textarea>'
				+'<button class="btn waves-effect" style="border-radius:0 50rem 50rem 0;background:var(--primary-color);padding-right:17px;color:white;"'
					+'onclick="newMessage(\'{{ID}}\');$(\'#chat-modal .messages-wrapper\').scrollTop($(\'#chat-modal .messages-wrapper\')[0].scrollHeight);">'
					+'<i class="fa fa-send m-0"></i>'
					+'</button>'
			+'</div>'
		+'</div>'
	+'</div>'
);

Handlebars.registerPartial(
	"message",
		'{{#if (eq Purpose "CONTENT")}}'
			+'<div class="message \{{#if (eq Direction \'incoming\')}}incoming\{{else}}outgoing\{{/if}}" data-message-id="{{MessageID}}">'
				+'<div class="d-flex flex-column">'
					+'<div class="message-sender">'
						+'\{{GivenName}}'
					+'</div>'
					+'<div class="message-date">'
						+'\{{relativeMoment Timestamp format="MMM DD YYYY - HH:mm"}}'
					+'</div>'
					+'<div class="message-text">{{Content}}</div>'
				+'</div>'
			+'</div>'
			+'{{else if (eq Purpose "NTRUPUBKEY")}}'
				+'<div class="message \{{#if (eq Direction \'incoming\')}}incoming\{{else}}outgoing\{{/if}}" data-message-id="{{MessageID}}">'
					+'<div class="d-flex flex-column">'
						+'<div class="message-sender">'
							+'\{{GivenName}}'
						+'</div>'
						+'<div class="message-date">'
							+'\{{relativeMoment Timestamp format="MMM DD YYYY - HH:mm"}}'
						+'</div>'
							+'<div class="message-info">(subscriber added)</div>'
					+'</div>'
				+'</div>'
			+'{{else if (eq Purpose "QUOTE")}}'
				+'<div class="message \{{#if (eq Direction \'incoming\')}}incoming\{{else}}outgoing\{{/if}}" data-message-id="{{MessageID}}">'
					+'<div class="d-flex flex-column">'
						+'<div class="message-sender">'
							+'\{{GivenName}}'
						+'</div>'
						+'<div class="message-date">'
							+'\{{relativeMoment Timestamp format="MMM DD YYYY - HH:mm"}}'
						+'</div>'
							+'<div class="message-text" data-reference-id="{{Reference}}">{{truncate Content 70}}</div>'
					+'</div>'
				+'</div>'
			+'{{else if (eq Purpose "REACTION")}}'
				+'<div class="message \{{#if (eq Direction \'incoming\')}}incoming\{{else}}outgoing\{{/if}}" data-message-id="{{MessageID}}">'
					+'<div class="d-flex flex-column">'
						+'<div class="message-sender">'
							+'\{{GivenName}}'
						+'</div>'
						+'<div class="message-date">'
							+'\{{relativeMoment Timestamp format="MMM DD YYYY - HH:mm"}}'
						+'</div>'
							+'<div class="message-text" data-reference-id="{{Reference}}">{{truncate Content 70}}</div>'
					+'</div>'
				+'</div>'
			+'{{else if (eq Purpose "PARTICIPANTINTRODUCTION")}}'
				+'{{#if (eq Direction "incoming") }}'
					+'<div class="message \{{#if (eq Direction \'incoming\')}}incoming\{{else}}outgoing\{{/if}}" data-message-id="{{MessageID}}">'
						+'<div class="d-flex flex-column">'
							+'<div class="message-sender">'
								+'\{{GivenName}}'
							+'</div>'
							+'<div class="message-date">'
								+'\{{relativeMoment Timestamp format="MMM DD YYYY - HH:mm"}}'
							+'</div>'
							+'{{#if (gt (count (lookup (decode Content) "Participants")) 1) }}'
								+'<div class="d-flex text-center" style="flex-direction: column">'
									+'<i class="fa fa-users fa-3x mr-3 my-auto" style="color: var(--primary-color)"></i>'
									+'<div class="">'
										+'<div>{{GivenName}} shared group contacts</div>'
										+'<button class="btn btn-sm btn-info" onclick="event.stopPropagation();viewInvitations(\'{{ID}}\',\'{{MessageID}}\');">See Contacts &amp; Manage Trust</button>'
									+'</div>'
								+'</div>'
							+'{{else}}'
								+'<div class="d-flex" style="flex-direction: column">'
									+'<div class="mb-2">Introducing new participant:</div>'
									+'<div class="d-flex text-center">'
										+'<i class="fa fa-user fa-2x mr-3 my-auto" style="color: var(--primary-color)"></i>'
										+'<span class="h6 font-weight-bold my-auto">{{ lookup (first (lookup (decode Content) "Participants")) "GivenName" }}</span>'
										+'<button class="btn btn-sm btn-info ml-auto my-auto" onclick="event.stopPropagation();acceptIntroduction(\'{{ID}}\',\'{{MessageID}}\', [0]);">Trust This Contact</button>'
										+'<i class="fa fa-2x fa-question-circle ml-2 my-auto" data-toggle="tooltip" data-placement="top" title="This participant can not read your group messages until you grant trust."></i>'
									+'</div>'
								+'</div>'
							+'{{/if}}'
						+'</div>'
					+'</div>'
					+'{{/if}}'
			+'{{/if}}'
);

Handlebars.registerPartial(
	"quote-response",
	'<div class="quote waves-effect" '
	  +'onclick="event.stopPropagation();openReactions($(\'#chat-modal\').attr(\'data-chat-id\'), $(this).parent().attr(\'data-reference-id\'));">'
		+'<div class="font-weight-bold small">\{{GivenName}}:</div>'
		+'\{{truncate Content 90 "..."}}'
	+'</div>'
	+'\{{#if (eq OriginalText "agree")}} <i class="fa fa-thumbs-up fa-3x"></i>'
	+'\{{else if (eq OriginalText "disagree")}} <i class="fa fa-thumbs-down fa-3x"></i> \{{else}} \{{truncate OriginalText 90 "..."}} \{{/if}}'
);


Handlebars.registerPartial(
	"reactions-modal",
	'<div class="d-flex flex-column">'
	  +'<div class="d-flex mb-2">'
	  	+'<div class="font-weight-bold">\{{GivenName}}:</div>'
	  	+'<div class="ml-auto small">\{{moment Timestamp format="MMM DD YYYY - HH:mm"}}</div>'
		+'</div>'
		+'<div class="message-content">\{{Content}}</div>'
		+'\{{#if (eq Purpose "CONTENT")}}'
			+'\{{> reaction-buttons}}'
		+'\{{/if}}'
		+'<button class="btn btn-secondary ml-auto" onclick="$(\'#reactions-modal\').modal(\'hide\');">Close</button>'
	+'</div>'
);

Handlebars.registerPartial(
	"reaction-buttons",
			'<div class="d-flex my-3 px-3 pt-3">'
				+'<button onclick="newReaction(\'agree\', $(\'#reactions-modal\').attr(\'data-chat-id\'), \'\{{MessageID}}\');" class="btn d-flex flex-column justify-content-center align-items-center  secondary-color white-text z-depth-1 hoverable w-100 waves-effect">'
					+'<i class="fa fa-thumbs-up fa-3x m-0 pb-1"></i>'
						+'<span class="font-weight-bold small">Agree</span>'
					+'</button>'
					+'<button onclick="newReaction(\'disagree\', $(\'#reactions-modal\').attr(\'data-chat-id\'), \'\{{MessageID}}\');" class="btn d-flex flex-column justify-content-center align-items-center  secondary-color white-text z-depth-1 hoverable w-100 waves-effect">'
						+'<i class="fa fa-thumbs-down fa-3x m-0 pb-1"></i>'
						+'<span class="font-weight-bold small">Disagree</span>'
					+'</button>'
					+'<button onclick="newReply($(\'#reactions-modal\').attr(\'data-chat-id\'), \'\{{MessageID}}\');" class="btn d-flex flex-column justify-content-center align-items-center  secondary-color white-text z-depth-1 hoverable w-100 waves-effect">'
						+'<i class="fa fa-reply fa-3x m-0 pb-1"></i>'
						+'<span class="font-weight-bold small">Quote/Reply</span>'
					+'</button>'
			+'</div>'
);



Handlebars.registerPartial(
	'invitations-modal',
	'<div class="d-flex flex-column p-5">'
		+'<i class="fa fa-5x fa-users mb-2 mx-auto"></i>'
	  +'<div class="d-flex mb-2"><div class="h4 my-auto text-center w-100"><span style="color:var(--primary-color);font-size:150%;">'
			+'{{GivenName}}</span> shared <span style="color:var(--primary-color);font-size:150%;">{{ count Invitations.Participants }}</span> Participants</div></div>'
		+'<p class="text-center mb-1">Below is a list of Participants which you <b>have not trusted yet</b>.</p>'
		+'<p class="text-center mb-5">New participants can not read your messages until you trust them.</p>'
		+'<div id="invitations-wrapper">'
			+'\{{#each Invitations.Participants}}'
			  +'{{#unless (eq Channel ../PublisherActiveChannel)}}'
					+'{{#unless (includes ../SubscriberIDs Channel)}}'
						+'<div class="d-flex mb-2">'
							+'<div class="invitation d-flex w-100">'
								+'<div class="h5 font-weight-bold my-auto mr-2"  style="border-bottom:1px solid var(--primary-color)">{{GivenName}}</div>'
								+'<button onclick="acceptIntroduction(\'{{../chatID}}\',\'{{../MessageID}}\', [{{@index}}]);$(this).parent().remove();$(\'.tooltip\').remove();" '
									+'class="btn btn-success ml-auto my-auto" data-toggle="tooltip" data-placement="top" title="This participant will be able to read your group messages"'
										+'><i class="fa fa-check mr-2 my-auto"></i>Trust</button>'
							+'</div>'
						+'</div>'
					+'\{{/unless}}'
				+'\{{/unless}}'
			+'\{{/each}}'
		+'</div>'
		+'<div class="d-flex mt-5">'
			//+'<button type="button" onclick="$(\'#invitations-modal\').modal(\'hide\');" class="btn btn-primary mr-auto waves-effect">'
					//+'<i class="fa fa-handshake-o mr-2"></i><span>Trust All</span></button>'
			+'<button type="button" onclick="$(\'#invitations-modal\').modal(\'hide\');" class="btn btn-secondary ml-auto waves-effect">'
					+'<i class="fa fa-close mr-2"></i><span>Close</span></button>'
		+'</div>'
	+'</div>'
);


Handlebars.registerPartial(
	'chat-settings-modal',
	'<div class="container-fluid">'
		  +'<h3 class="mb-3">Chat Name</h3>'
			+'<div class="d-flex flex-column">'
				+'<div class="d-flexpr-5 mr-5">'
					+'<input id="chat-name" type="text" class="w-100" value="\{{GivenName}}" onchange="s2.S2.Chats.{{ID}}.GivenName = $(this).val()">'
				+'</div>'
				+'<div class="d-flex flex-column mt-5">'
				  +'<h4>My Secret <i class="fa fa-question-circle primary-text ml-2" data-toggle="tooltip" data-placement="top" title="Send your Secret to anyone you want to invite to this chat."></i></h4>'
					+'<p id="my-secret" class="waves-effect" onclick="toClipboard(\'\{{ID}}\')">\{{ID}}</p>'
					+'<div class="d-flex mb-5">'
						+'<button type="button" class="btn btn-success mr-auto" onclick="sendEmailInvite(\'\{{ID}}\')">'
								+'<i class="fa fa-paper-plane mr-2"></i><span>Send Via Email</span></button>'
						+'<button type="button" class="btn btn-info" onclick="toClipboard(\'\{{ID}}\')">'
								+'<i class="fa fa-clipboard mr-2"></i><span>Copy Secret</span></button>'
					+'</div>'
					+'<div id="active-channel-qr" class="mb-4"></div>'
		+'</div>'
		+'<div class="d-flex w-100">'
			+'<button type="button" class="btn btn-danger" onclick="if(confirm(\'Are you sure you want to delete this chat ? This action cannot be undone!\')) { deleteChat(\'\{{ID}}\') }">'
					+'<i class="fa fa-trash mr-2"></i><span>Delete Chat</span></button>'
			+'<button type="button" onclick="saveChatSettings(\'\{{ID}}\');$(\'#chat-settings-modal\').modal(\'hide\');" class="btn btn-secondary waves-effect ml-auto">'
					+'<i class="fa fa-window-close mr-2"></i><span>Close</span></button>'
		+'</div>'
	+'</div>'
	);

	Handlebars.registerPartial(
		'participants-modal',
		'<div class="container-fluid">'
					+'<h3 class="mb-3">Participants</h3>'
					+'{{#unless Subscribers}}&nbsp;&nbsp;- No Participants Yet{{/unless}}'
					+'\{{#each Subscribers as |subscriber|}}'
						+'<div class="subscriber mb-3">'
						  +'<div class="d-flex">'
								+'<h6 class="m-0" contenteditable="true" oninput="s2.S2.Chats.{{../ID}}.Subscribers[{{@index}}].GivenName = $(this).text()">{{GivenName}}</h6>'
								+'<i class="fa fa-question-circle primary-text ml-2 my-auto waves-effect" data-toggle="tooltip" data-placement="bottom" title="You can edit the participant name on the left. Click this icon to copy the participant SECRET." onclick="toClipboard(\'{{Basechannel}}\')"></i>'
								+'<i class="fa fa-close text-danger ml-auto waves-effect" data-toggle="tooltip" data-placement="bottom" title="Remove Participant from Channel" onclick="if (confirm(\'Do you really want to remove participant `{{GivenName}}`?\\nThis action cannot be undone!\')) { deleteSubscriber(\'\{{../ID}}\', s2.S2.Chats.{{../ID}}.Subscribers[{{@index}}].Basechannel) }"></i>'
							+'</div>'
							+'<div class="subscriberapi"></div>'
						+'</div>'
					+'\{{/each}}'
					+'<h4 class="mt-5">Add New Participant <i class="fa fa-question-circle primary-text ml-2" data-toggle="tooltip" data-placement="top" title="Enter the name and Secret of a participant."></i></h4>'
					+'<input id="newsubscribername" class="w-100 mb-2" type="text" minlength="81" maxlength="81" placeholder="Participant Name">'
					+'<div class="d-flex flex-column flex-md-row">'
						+'<input id="newsubscriberchannel" class="w-100" type="text" minlength="81" maxlength="81" placeholder="Participant Secret">'
						+'<button type="button" class="btn btn-info" onclick="scanQRCode(\'\{{ID}}\')" style="white-space: nowrap;">'
								+'<i class="fa fa-camera mr-2"></i><span>Scan QR Code</span></button>'
					+'</div>'
					+'<div id="loadingMessage" hidden style="font-size:14px;margin-top:1rem;" >🎥 Cannot access video stream (make sure you have a webcam and SSL enabled)</div>'
	  			+'<canvas id="canvas" hidden></canvas>'
					+'<div id="output" hidden style="margin-top:1rem;">'
				    +'<div id="outputMessage">No QR code detected.</div>'
				    +'<div hidden><b>Data:</b> <span id="outputData"></span></div>'
				  +'</div>'
					+'<div class="btn-group mt-3">'
						+'<button type="button" class="btn btn-warning" onclick="newSubscriber(\'\{{ID}}\');">'
								+'<i class="fa fa-plus mr-2"></i><span>Add</span></button>'
					+'</div>'
				+'</div>'
				+'<div class="d-flex w-100 mt-3">'
					+'<button type="button" onclick="saveParticipants(\'\{{ID}}\');" class="btn btn-secondary waves-effect ml-auto">'
							+'<i class="fa fa-check mr-2"></i><span>Close</span></button>'
				+'</div>'
	    +'</div>'
	);

	Handlebars.registerPartial(
		'add-participant-modal',
		'<div class="container-fluid">'
					+'<h3 class="mb-3">Add Participant to {{GivenName}}</h3>'
					+'<div id="add-participant-carousel" class="carousel slide" data-ride="carousel">'
					  +'<div class="carousel-inner">'
					    +'<div class="carousel-item active">'
					      +'<h5 class="font-weight-bold mt-3">STEP 1:</h5>'
								+'<h6 class="mt-3">Share Your Secret with the new participant <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Send your Secret to anyone you want to invite to this chat."></i></h6>'
								+'<p id="my-secret" class="waves-effect" onclick="toClipboard(\'\{{ID}}\')">\{{ID}}</p>'
								+'<div class="d-flex mb-5">'
									+'<button type="button" class="btn btn-success mr-auto" onclick="sendEmailInvite(\'\{{ID}}\')">'
											+'<i class="fa fa-paper-plane mr-2"></i><span>Send Via Email</span></button>'
									+'<button type="button" class="btn btn-info" onclick="toClipboard(\'\{{ID}}\')">'
											+'<i class="fa fa-clipboard mr-2"></i><span>Copy Secret</span></button>'
								+'</div>'
								+'<div id="active-channel-qr" class="mb-4"></div>'
								+'<div class="d-flex">'
									+'<button type="button" class="btn btn-success ml-auto" onclick="$(\'#add-participant-carousel\').carousel(\'next\');" style="white-space: nowrap;">'
											+'<i class="fa fa-chevron-right mr-2"></i><span>Next</span></button>'
								+'</div>'
					    +'</div>'
					    +'<div class="carousel-item">'
								+'<h5 class="font-weight-bold mt-3">STEP 2:</h5>'
								+'<h6 class="font-weight-bold mt-3">Enter Participant Secret <i class="fa fa-question-circle primary-text ml-2"></i></h6>'
								+'<input id="newsubscribername" class="w-100 mb-2" type="text" minlength="81" maxlength="81" placeholder="Participant Name">'
								+'<div class="d-flex flex-column flex-md-row">'
									+'<input id="newsubscriberchannel" class="w-100" type="text" minlength="81" maxlength="81" placeholder="Participant Secret">'
									+'<button type="button" class="btn btn-info" onclick="scanQRCode(\'\{{ID}}\')" style="white-space: nowrap;">'
											+'<i class="fa fa-camera mr-2"></i><span>Scan QR Code</span></button>'
								+'</div>'
								+'<div id="loadingMessage" hidden style="font-size:14px;margin-top:1rem;" >🎥 Cannot access video stream (make sure you have a webcam and SSL enabled)</div>'
								+'<canvas id="canvas" hidden></canvas>'
								+'<div id="output" hidden style="margin-top:1rem;">'
									+'<div id="outputMessage">No QR code detected.</div>'
									+'<div hidden><b>Data:</b> <span id="outputData"></span></div>'
								+'</div>'
								+'<div class="d-flex flex-column flex-md-row mt-4">'
									+'<button type="button" class="btn btn-success" onclick="$(\'#add-participant-carousel\').carousel(\'prev\');" style="white-space: nowrap;">'
											+'<i class="fa fa-chevron-left mr-2"></i><span>Go Back</span></button>'
									+'<button type="button" class="btn btn-success ml-auto" onclick="newSubscriber(\'{{ID}}\');" style="white-space: nowrap;">'
											+'<i class="fa fa-chevron-right mr-2"></i><span>Next</span></button>'
								+'</div>'
					    +'</div>'
					    +'<div class="carousel-item">'
					      +'<h5 class="font-weight-bold mt-3">STEP 3: Done!</h5>'
								+'<div>The new participant has been added successfully !</div>'
								+'<div class="d-flex mt-4">'
									+'<div class="btn btn-info waves-effect" onclick="$(\'#add-participant-carousel\').carousel(\'dispose\'); $(\'#add-participant-modal\').modal(\'hide\');editParticipants(\'{{ID}}\', true)" data-toggle="tooltip" data-placement="left"><i class="fa fa-users m-0 mr-2"></i>Manage Active Participants</div>'
									+'<button type="button" onclick="$(\'#add-participant-carousel\').carousel(\'dispose\'); $(\'#add-participant-modal\').modal(\'hide\');" class="btn btn-secondary waves-effect ml-auto"><i class="fa fa-check mr-2"></i><span>Close</span></button>'
								+'</div>'
							+'</div>'
					  +'</div>'
				+'</div>'
	    +'</div>'
	);
